###spring boot 整合mybatis
1. 在springboot项目的pom中添加以下依赖,mysql-connector与mybatis-spring-boot-starter
```xml
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
		</dependency>

		<dependency>
			<groupId>org.mybatis.spring.boot</groupId>
			<artifactId>mybatis-spring-boot-starter</artifactId>
			<version>2.0.1</version>
		</dependency>
```
2. 在数据库中创建测试表:
```sql
CREATE TABLE `test`.`t_user` (
  `F_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `F_username` varchar(32) DEFAULT NULL,
  `F_password` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`F_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
3. dao包下的类:
UserMapper通过注解方式写SQL实现数据库操作
UserMapperXml通过xml文件写SQL实现数据库操作

```java
@Mapper
public interface UserMapper {
	@Select("select * from test.t_user where F_user_id=#{id}")
	//结果实体类字段与数据库字段映射
	@Results(value = { @Result(property = "FUserId", column = "F_user_id"),
			@Result(property = "FUsername", column = "F_username"),
			@Result(property = "FPassword", column = "F_password") })
	public User select(@Param("id") Integer id);

	@Insert("insert into test.t_user (F_username,F_password) values(#{user.FUsername},#{user.FPassword})")
	public Integer save(@Param("user") User user);

	@Update("update test.t_user set F_username = #{uname} where F_user_id=#{id}")
	@Options(useGeneratedKeys = true)
	public Integer update(@Param("uname") String uname, @Param("id") Integer id);
}

//通过xml文件写SQL
public interface UserMapperXml {
	public User select(@Param("id") Integer id);

	public Integer save(@Param("user") User user);

	public Integer update(@Param("uname") String uname, @Param("id") Integer id);
}
```
4. UserMapperXml.java接口对应的xml文件

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >

<!-- namespace对应的是接口名称 -->
<mapper namespace="com.harris.spring_boot_study.dao.UserMapperXml">
	<!-- 字段与实体类的对应,后续的select操作可以返回 resultMap -->
	<resultMap id="BaseResultMap" type="com.harris.spring_boot_study.domain.User">
		<id column="F_user_id" property="FUserId" jdbcType="INTEGER" />
		<result column="F_username" property="FUsername" jdbcType="VARCHAR" />
		<result column="F_password" property="FPassword" jdbcType="VARCHAR" />
	</resultMap>
	<!-- 字段集,在SQL中可以引用 -->
	<sql id="Base_Column_List">
		F_user_id, F_username, F_password
	</sql>
	<!-- 对应UserMapperXml中的select方法,返回BaseResultMap,即User实体类,接受参数为integer类型 -->
	<select id="select" resultMap="BaseResultMap" parameterType="java.lang.Integer">
		select
		<include refid="Base_Column_List" />
		from test.t_user where F_user_id=#{id}
	</select>
	<!-- 对应UserMapperXml中的select方法,接受参数为User类型,useGeneratedKeys表示返回数据库keyColumn的值到keyProperty,数据库字段名为F_user_id,实体类字段名为FUserId -->
	<insert id="save" parameterType="com.harris.spring_boot_study.domain.User"
		useGeneratedKeys="true" keyColumn="F_user_id" keyProperty="FUserId">
		insert into
		test.t_user (F_username,F_password)
		values(#{user.FUsername},#{user.FPassword})
	</insert>
	<!-- 对应UserMapperXml中的update方法 -->
	<update id="update">
		update test.t_user set F_username = #{uname} where
		F_user_id=#{id}
	</update>
</mapper>
```

domain包下实体类
```java
public class User {
	private Integer FUserId;
	private String FUsername;
	private String FPassword;
	//getter..setter
}
```
启动类App
```java
@SpringBootApplication
@RestController
@ComponentScan("com.harris")
@MapperScan("com.harris.spring_boot_study.dao")
public class App {
	@Resource
	private UserMapperXml userMapperXml;

	@RequestMapping("/")
	@Transactional
	public String rest(Integer id) {
		System.out.println("param is " + id);
		//从数据库中查询用户信息
		User user = userMapperXml.select(id);
		System.out.println("user is " + user.toString());
	
		//新增,更新数据
		String uname = user.getFUsername();
		user.setFUsername(id + uname);
		user.setFPassword("password" + id);
		System.out.println("insert row affect :" + userMapperXml.save(user));
		System.out.println("update row affect :" + userMapperXml.update(uname, id));
		return "Hello " + uname;
	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
```
applocation.properties
```properties
server.port=8888
# DataSource连接配置
spring.datasource.url=jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8
spring.datasource.driverClassName=com.mysql.cj.jdbc.Driver
spring.datasource.username=root
spring.datasource.password=123456

# mybatis的xml地址配置
mybatis.mapperLocations=classpath*:mapper*/*.xml

```
启动服务后执行命令
```shell
curl 'http://localhost:8888?id=6'
返回: Hello user1
```
服务器日志:
param is 6
user is {"fPassword":"pwd","fUserId":6,"fUsername":"user1"}
insert row affect :1
update row affect :1


###整合机制启动机制
引入mybatis-spring-boot-starter可以帮我们自动配置mybatis
start的pom依赖关系:
![mybatis_starter_dependencies](pic/mybatis_starter_dependencies.png)
starter依赖spring-boot-starter,spring-boot-starter-jdbc,mybatis-spring-boot-autoconfigure,mybatis,mybatis-spring
主要看mybatis-spring-boot-autoconfigure,帮我们自动配置mybatis
1. 在mybatis-spring-boot-autoconfigure的jar包下META-INF/spring.factories下的配置变量,表示在spring启动时候会自动加载这个配置类.
```properties
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration
```

####MybatisAutoConfiguration
这个类主要配置了几个bean:SqlSessionFactory,SqlSessionTemplate
```java
@org.springframework.context.annotation.Configuration
//依赖两个类SqlSessionFactory.class, SqlSessionFactoryBean.class
@ConditionalOnClass({ SqlSessionFactory.class, SqlSessionFactoryBean.class })
//只有一个DataSource的bean,或者有多个的时候指定一个primary candidate时满足条件
@ConditionalOnSingleCandidate(DataSource.class)
//mybatis开头的变量配置
@EnableConfigurationProperties(MybatisProperties.class)
//数据源配置成功后读取这个配置类
@AutoConfigureAfter(DataSourceAutoConfiguration.class)
public class MybatisAutoConfiguration implements InitializingBean {
  //mybatis配置
  private final MybatisProperties properties;
  //拦截器
  private final Interceptor[] interceptors;
  //资源
  private final ResourceLoader resourceLoader;
  // 数据库id,xml可以对于不同的数据库id提供不同的SQL,如oracle,mysql等等
  private final DatabaseIdProvider databaseIdProvider;
  //自定义配置
  private final List<ConfigurationCustomizer> configurationCustomizers;

  //配置SqlSessionFactory,SqlSessionFactory是SqlSession(执行SQL方法)的工厂类,关联每个Configuration配置
  @Bean
  @ConditionalOnMissingBean
  public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
  
  }
  
  //配置SqlSessionTemplate,SqlSessionTemplate是Mybatis为了接入Spring提供的Bean。通过TransactionSynchronizationManager中的ThreadLocal<Map<Object, Object>>保存线程对应的SqlSession，实现session的线程安全
  @Bean
  @ConditionalOnMissingBean
  public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
    ExecutorType executorType = this.properties.getExecutorType();
    if (executorType != null) {
      return new SqlSessionTemplate(sqlSessionFactory, executorType);
    } else {
      return new SqlSessionTemplate(sqlSessionFactory);
    }
  }

  //配置类,在没有MapperFactoryBean时候启用此配置,如果在配置类上使用了MapperScan注解,MapperScan会为接口生成MapperFactoryBean,则不会启用此配置,此配置类扫描包下的Mapper注解,生成对应的MapperFactoryBean
  @org.springframework.context.annotation.Configuration
  @Import({ AutoConfiguredMapperScannerRegistrar.class })
  @ConditionalOnMissingBean(MapperFactoryBean.class)
  public static class MapperScannerRegistrarNotFoundConfiguration implements InitializingBean {

    @Override
    public void afterPropertiesSet() {
      logger.debug("No {} found.", MapperFactoryBean.class.getName());
    }
  }
}
```
 
####MapperScan注解
MapperScan注解作用是扫描指定的包,将包下的接口生成对应的MapperFactoryBean注册到spring容器中,注解Import(MapperScannerRegistrar.class,该类实现了ImportBeanDefinitionRegistrar),spring容器启动时会调用registerBeanDefinitions方法注册bean
```java
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
//MapperScannerRegistrar处理类
@Import(MapperScannerRegistrar.class)
@Repeatable(MapperScans.class)
public @interface MapperScan {
  //扫描的包名,跟basePackages一样
  //*代表任意一个包名称(如:com.*.mapper,可以扫描com.xx.mapper但不能扫描com.xx.yy.mapper)
  //**代表任意路径任意包名(如:com.**.mapper,可以扫描com.xx.mapper但也能扫描com.xx.yy.mapper)
  String[] value() default {};
  //扫描的包名,跟value一样
  String[] basePackages() default {};
  //扫描指定类所在的包名
  Class<?>[] basePackageClasses() default {};
  //生成bean的名称规则
  Class<? extends BeanNameGenerator> nameGenerator() default BeanNameGenerator.class;
  //扫描指定的注解
  Class<? extends Annotation> annotationClass() default Annotation.class;
  //扫描指定的接口类
  Class<?> markerInterface() default Class.class;
  //如果有多个sqlSessionTemplate的话,关联指定的sqlSessionTemplate
  String sqlSessionTemplateRef() default "";
  //如果有多个sqlSessionFactory的话,关联指定的sqlSessionFactory
  String sqlSessionFactoryRef() default "";
  //自定义MapperFactoryBean,用于创建bean的实例工厂
  Class<? extends MapperFactoryBean> factoryBean() default MapperFactoryBean.class;
}
```
MapperScannerRegistrar调用ClassPathMapperScanner进行扫描并注册bean到spring容器中
bean处理方式
org.mybatis.spring.mapper.ClassPathMapperScanner.processBeanDefinitions(Set<BeanDefinitionHolder>)
这个方法将扫描到的接口bean定义,设置接口对应的beanclass=MapperFactoryBean.class,设置属性addToConfig,sqlSessionFactory,sqlSessionTemplate等

####接口对应的bean初始化
初始化MybatisAutoConfiguration.sqlSessionFactory时候会调用org.mybatis.spring.SqlSessionFactoryBean.buildSqlSessionFactory()创建SqlSessionFactory,利用XMLMapperBuilder.parse解析xml,将解析的结果放到一个`HashMap<Class<?>, MapperProxyFactory<?>>`上,位于org.apache.ibatis.binding.MapperRegistry.knownMappers,key为接口的类,value为MapperProxyFactory
MapperRegistry位于Configuration中org.apache.ibatis.session.Configuration.mapperRegistry

非xml方式初始化:
如果我们使用@Mapper注解接口方式,如上例的UserMapper,初始化工作会在接口对应的MapperFactoryBean的afterPropertiesSet方法中,此方法会检查对应的接口是否存在Configuration.mapperRegistry中,如果不存在的话则添加到Configuration.mapperRegistry

####dao注入对应的字段中
在spring容器发现依赖某个dao时,从容器中找出对应的MapperFactoryBean,由于这是一个FactoryBean,会调用其getObject()方法,使用JDK动态代理创建接口的实现类,代理方法会转发到org.apache.ibatis.binding.MapperProxy.invoke(Object, Method, Object[])去执行.
```java
 public class MapperFactoryBean<T> extends SqlSessionDaoSupport implements FactoryBean<T> {
	//省略其他方法
    //getSqlSession()返回的是sqlSessionTemplate
	@Override
	public T getObject() throws Exception {
	  return getSqlSession().getMapper(this.mapperInterface);
	}
}

  //org.mybatis.spring.SqlSessionTemplate.getMapper(Class<T>)
  @Override
  public <T> T getMapper(Class<T> type) {
    return getConfiguration().getMapper(type, this);
  }

  //Configuration的getMapper会调用 org.apache.ibatis.binding.MapperRegistry.getMapper(Class<T>, SqlSession)
  //从knownMappers拿到之前注册的MapperProxyFactory,然后调用为接口创建动态代理,
  public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
    final MapperProxyFactory<T> mapperProxyFactory = (MapperProxyFactory<T>) knownMappers.get(type);
    if (mapperProxyFactory == null) {
      throw new BindingException("Type " + type + " is not known to the MapperRegistry.");
    }
    try {
      return mapperProxyFactory.newInstance(sqlSession);
    } catch (Exception e) {
      throw new BindingException("Error getting mapper instance. Cause: " + e, e);
    }
  }

  //MapperProxyFactory使用JDK动态代理创建dao接口的代理类,MapperProxy实现了InvocationHandler,调用接口的方法会代理到MapperProxy中去
  public T newInstance(SqlSession sqlSession) {
    final MapperProxy<T> mapperProxy = new MapperProxy<>(sqlSession, mapperInterface, methodCache);
    return newInstance(mapperProxy);
  }

  protected T newInstance(MapperProxy<T> mapperProxy) {
    return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface }, mapperProxy);
  }
```
生成的代理类如下
```java
public final class $Proxy62 extends Proxy
  implements UserMapperXml{

  private static Method m1;
  private static Method m2;
  private static Method m5;
  private static Method m4;
  private static Method m3;
  private static Method m0;
  
  //InvocationHandler为MapperProxy
  public $Proxy62(InvocationHandler paramInvocationHandler){
    super(paramInvocationHandler);
  }

  public final User select(Integer paramInteger){
	//m5为接口对应的select方法,实际上调用的是MapperProxy.invoke方法
    return (User)this.h.invoke(this, m5, new Object[] { paramInteger });
  }
  //方法变量初始化
  static{
      m1 = Class.forName("java.lang.Object").getMethod("equals", new Class[] { Class.forName("java.lang.Object") });
      m2 = Class.forName("java.lang.Object").getMethod("toString", new Class[0]);
      m5 = Class.forName("com.harris.spring_boot_study.dao.UserMapperXml").getMethod("select", new Class[] { Class.forName("java.lang.Integer") });
      m4 = Class.forName("com.harris.spring_boot_study.dao.UserMapperXml").getMethod("save", new Class[] { Class.forName("com.harris.spring_boot_study.domain.User") });
      m3 = Class.forName("com.harris.spring_boot_study.dao.UserMapperXml").getMethod("update", new Class[] { Class.forName("java.lang.String"), Class.forName("java.lang.Integer") });
      m0 = Class.forName("java.lang.Object").getMethod("hashCode", new Class[0]);
  }
}
```

####接口执行过程
当我们在service调用dao时候,调用会转发到MapperProxy.invoke中执行.
```java
  //org.apache.ibatis.binding.MapperProxy.invoke(Object, Method, Object[])方法实现
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    //省略Object方法与接口中的default方法,都是直接调用
	//缓存方法,缓存到ConcurrentHashMap<Method, MapperMethod> methodCache
    final MapperMethod mapperMethod = cachedMapperMethod(method);
	//执行对应的方法,执行对应的insert,update,selcet,delete操作,其中sqlSession的实现类为配置类中定义的bean:SqlSessionTemplate
    return mapperMethod.execute(sqlSession, args);
  }

  //执行数据库操作,调用的是SqlSession的增删查改接口,spring中也就是调用SqlSessionTemplate的增删查改
  public Object execute(SqlSession sqlSession, Object[] args) {
    Object result;
    switch (command.getType()) {
      case INSERT: {
        Object param = method.convertArgsToSqlCommandParam(args);
        result = rowCountResult(sqlSession.insert(command.getName(), param));
        break;
      }
      case UPDATE: {
        Object param = method.convertArgsToSqlCommandParam(args);
        result = rowCountResult(sqlSession.update(command.getName(), param));
        break;
      }
      case DELETE: {
        Object param = method.convertArgsToSqlCommandParam(args);
        result = rowCountResult(sqlSession.delete(command.getName(), param));
        break;
      }
      case SELECT:
        if (method.returnsVoid() && method.hasResultHandler()) {
          executeWithResultHandler(sqlSession, args);
          result = null;
        } else if (method.returnsMany()) {
          result = executeForMany(sqlSession, args);
        } else if (method.returnsMap()) {
          result = executeForMap(sqlSession, args);
        } else if (method.returnsCursor()) {
          result = executeForCursor(sqlSession, args);
        } else {
          Object param = method.convertArgsToSqlCommandParam(args);
          result = sqlSession.selectOne(command.getName(), param);
          if (method.returnsOptional()
              && (result == null || !method.getReturnType().equals(result.getClass()))) {
            result = Optional.ofNullable(result);
          }
        }
        break;
      case FLUSH:
        result = sqlSession.flushStatements();
        break;
      default:
        throw new BindingException("Unknown execution method for: " + command.getName());
    }
    if (result == null && method.getReturnType().isPrimitive() && !method.returnsVoid()) {
      throw new BindingException("Mapper method '" + command.getName()
          + " attempted to return null from a method with a primitive return type (" + method.getReturnType() + ").");
    }
    return result;
  
  }
```

SqlSessionTemplate为SqlSession的静态代理类,实际并不执行数据库操作,委托到实际的SqlSession执行,sqlSessionProxy为动态代理对象,所有调用SqlSession的方法会转发到SqlSessionInterceptor.invoke执行,也就是被SqlSessionInterceptor拦截
```java
public class SqlSessionTemplate implements SqlSession, DisposableBean {
  //SqlSessionFactory,为DefaultSqlSessionFactory
  private final SqlSessionFactory sqlSessionFactory;
  //执行类型(SIMPLE, REUSE, BATCH)
  private final ExecutorType executorType;
  //动态代理对象,所有调用SqlSession的方法会转发到SqlSessionInterceptor.invoke执行
  private final SqlSession sqlSessionProxy;
  //异常处理
  private final PersistenceExceptionTranslator exceptionTranslator;

  //构造函数,初始化的时候没指定ExecutorType时候默认为SIMPLE,MyBatisExceptionTranslator
  public SqlSessionTemplate(SqlSessionFactory sqlSessionFactory, ExecutorType executorType,
      PersistenceExceptionTranslator exceptionTranslator) {
        this.sqlSessionFactory = sqlSessionFactory;
    this.executorType = executorType;
    this.exceptionTranslator = exceptionTranslator;
	//sqlSessionProxy为生成动态代理类,调用SqlSession的方法会转发到SqlSessionInterceptor.invoke执行
    this.sqlSessionProxy = (SqlSession) newProxyInstance(
        SqlSessionFactory.class.getClassLoader(),
        new Class[] { SqlSession.class },
        new SqlSessionInterceptor());
  }
}
```
SqlSessionInterceptor为SqlSessionTemplate的内部类,拦截SqlSession接口的方法
```java
  private class SqlSessionInterceptor implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
	  // 获取实际执行数据库操作的SqlSession,默认为DefaultSqlSession
      SqlSession sqlSession = getSqlSession(
          SqlSessionTemplate.this.sqlSessionFactory,
          SqlSessionTemplate.this.executorType,
          SqlSessionTemplate.this.exceptionTranslator);
      try {
		//调用执行数据库操作的SqlSession方法,默认为DefaultSqlSession
        Object result = method.invoke(sqlSession, args);
		//如果不是spring管理的事务,手动提交一下
        if (!isSqlSessionTransactional(sqlSession, SqlSessionTemplate.this.sqlSessionFactory)) {
          // force commit even on non-dirty sessions because some databases require
          // a commit/rollback before calling close()
          sqlSession.commit(true);
        }
        return result;
      } catch (Throwable t) {
        Throwable unwrapped = unwrapThrowable(t);
	    //异常处理,关闭sqlSession,将sqlSession置为null
        throw unwrapped;
      } finally {
		//关闭sqlSession,如果是在事务中,也就是能从spring的线程本地变量resources获取到SqlSession,将SqlSessionHolder引用计数减1,如果不是在事务中,则关闭SqlSession
        if (sqlSession != null) {
          closeSqlSession(sqlSession, SqlSessionTemplate.this.sqlSessionFactory);
        }
      }
    }
  }

}
  //获取sqlSession,TransactionSynchronizationManager以ThreadLocal保存事务相关的资源信息
  //此方法先从TransactionSynchronizationManager的本地线程变量resources获取,如果为空则从sessionFactory开启一个(默认为DefaultSqlSession)并保存到TransactionSynchronizationManager的resources中
  public static SqlSession getSqlSession(SqlSessionFactory sessionFactory, ExecutorType executorType, PersistenceExceptionTranslator exceptionTranslator) {

    notNull(sessionFactory, NO_SQL_SESSION_FACTORY_SPECIFIED);
    notNull(executorType, NO_EXECUTOR_TYPE_SPECIFIED);

	//如果是在事务中,从线程本地变量的resources获取SqlSession,如果能获取到,则复用SqlSession.
    SqlSessionHolder holder = (SqlSessionHolder) TransactionSynchronizationManager.getResource(sessionFactory);
	//获取实际的SqlSession,SqlSessionHolder的引用值加1
    SqlSession session = sessionHolder(executorType, holder);
    if (session != null) {
      return session;
    }
	//开启新的SqlSession,如果开启了事务,则注册到线程本地变量resources中,用于以上代码复用SqlSession
    LOGGER.debug(() -> "Creating a new SqlSession");
    session = sessionFactory.openSession(executorType);

    registerSessionHolder(sessionFactory, executorType, exceptionTranslator, session);

    return session;
  }
```

###mybatis插件应用
mybatis通过实现org.apache.ibatis.plugin.Interceptor接口,并在对应的类实现上注解org.apache.ibatis.plugin.Intercepts
拦截器使用责任链方式调用,第一个拦截器保存实际方法本身,从第二个拦截器开始,每个拦截器保存上一个拦截器引用,执行的时候从最后一个开始遍历调用.
```java
//声明印个spring的组件
@Component
//拦截的方法签名,type为拦截接口类,支持对Executor、StatementHandler、PameterHandler和ResultSetHandler
@Intercepts({ @Signature(method = "prepare", type = StatementHandler.class, args = { Connection.class, Integer.class }),
		@Signature(method = "query", type = Executor.class, args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class }) })
public class MyInterceptor implements Interceptor {
	//拦截器具体逻辑
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		System.out.println("before MyInterceptor " + invocation.getTarget().getClass().getName() + " method : " + invocation.getMethod().getName());
		调用下一个拦截器或具体方法
		Object result = invocation.proceed();
		System.out.println("after MyInterceptor ");
		return result;
	}

	//返回拦截代理对象,如果只有一个插件的话,target为实际执行逻辑本身,将代理对象与插件自身传到Plugin.wrap生成动态代理对象
	// 如果有多个插件的话,这里target可能是上一个插件包装过的对象,由此形成链式,调用的时候根据链式方式调用.
	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
	}
}
```
####生成动态代理对象方法org.apache.ibatis.plugin.Plugin.wrap(Object, Interceptor)
Plugin实现了InvocationHandler接口,生成代理对象时候将本身作为InvocationHandler传入到Proxy中,调用插件的方法会转发到Plugin.invoke执行
```java
 public static Object wrap(Object target, Interceptor interceptor) {
    //获取拦截的方法,根据注解@Intercepts来获取方法签名
    Map<Class<?>, Set<Method>> signatureMap = getSignatureMap(interceptor);
    Class<?> type = target.getClass();
    Class<?>[] interfaces = getAllInterfaces(type, signatureMap);
	//生成动态代理对象,调用实际方法会被转发到此类Plugin.invoke执行
    if (interfaces.length > 0) {
      return Proxy.newProxyInstance(
          type.getClassLoader(),
          interfaces,
          new Plugin(target, interceptor, signatureMap));
    }
    return target;
  }
  
  // invoke方法
  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    try {
	  //获取注解@Intercepts声明的方法
	  //如果有声明拦截的话,调用拦截器的intercept方法,没有的话直接调用对象方法本身
	  //如果有多个拦截器的话,则跳过当前拦截器调用下一拦截器的此方法(假设下一拦截器有声明拦截相应的方法)
      Set<Method> methods = signatureMap.get(method.getDeclaringClass());
      if (methods != null && methods.contains(method)) {
        return interceptor.intercept(new Invocation(target, method, args));
      }
	  // 调用实际对象本身或下一invoke方法(即此方法),target可能是下一拦截器包装的对象.
      return method.invoke(target, args);
    } catch (Exception e) {
      throw ExceptionUtil.unwrapThrowable(e);
    }
  }
```

Mybatis支持对Executor、StatementHandler、PameterHandler和ResultSetHandler 接口进行拦截，也就是说会对这4种对象进行代理

org.apache.ibatis.plugin.Interceptor是mybatis的拦截器接口,里面定义了3个方法
```java
public interface Interceptor {

  //拦截器执行逻辑,可以在执行方法前后添加对应的逻辑
  Object intercept(Invocation invocation) throws Throwable;
  //是否代理对象
  Object plugin(Object target);
  //属性设置,可以在xml指定属性
  void setProperties(Properties properties);
}
```
####插件注册方式
我们将插件声明成Spring容器的bean,在初始化mybatis配置时候保存所有的插件到 MybatisAutoConfiguration 类中
```java
  public MybatisAutoConfiguration(MybatisProperties properties,
      ObjectProvider<Interceptor[]> interceptorsProvider,
      ResourceLoader resourceLoader,
      ObjectProvider<DatabaseIdProvider> databaseIdProvider,
      ObjectProvider<List<ConfigurationCustomizer>> configurationCustomizersProvider) {
    this.properties = properties;
	//保存所有mybatis插件
    this.interceptors = interceptorsProvider.getIfAvailable();
    this.resourceLoader = resourceLoader;
    this.databaseIdProvider = databaseIdProvider.getIfAvailable();
    this.configurationCustomizers = configurationCustomizersProvider.getIfAvailable();
  }
  //声明SqlSessionFactory时候保存插件信息到Configuration中去,插件信息会保存到 Configuration的InterceptorChain interceptorChain字段中
  @Bean
  @ConditionalOnMissingBean
  public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
    SqlSessionFactoryBean factory = new SqlSessionFactoryBean();
  	if (!ObjectUtils.isEmpty(this.interceptors)) {
      factory.setPlugins(this.interceptors);
    }
  }
```
插件使用的4个地方:位于Configuration配置类中

```
// 创建Executor
public Executor newExecutor(Transaction transaction, ExecutorType executorType) ```
```
// 创建StatementHandler
public StatementHandler newStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql)
```
```
// 创建ParameterHandler
public ParameterHandler newParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql)
```
```
// 创建ResultSetHandler
public ResultSetHandler newResultSetHandler(Executor executor, MappedStatement mappedStatement, RowBounds rowBounds, ParameterHandler parameterHandler,      ResultHandler resultHandler, BoundSql boundSql) ```

###缓存
mybatis有一级与二级缓存,二级缓存位于CachingExecutor,CachingExecutor包装了原先的Executor,另外有一个缓存的字段TransactionalCacheManager,根据每个MappedStatement传的是否有配置缓存,与SqlSession无关,从而实现各个SqlSession共享缓存.
```java
//Configuration.newExecutor
 public Executor newExecutor(Transaction transaction, ExecutorType executorType) {
    executorType = executorType == null ? defaultExecutorType : executorType;
    executorType = executorType == null ? ExecutorType.SIMPLE : executorType;
    Executor executor;
	//创建不同的Executor
    if (ExecutorType.BATCH == executorType) {
      executor = new BatchExecutor(this, transaction);
    } else if (ExecutorType.REUSE == executorType) {
      executor = new ReuseExecutor(this, transaction);
    } else {
      executor = new SimpleExecutor(this, transaction);
    }
	//使用缓存的话,cacheEnabled默认是true,用CachingExecutor包装原先的Executor
    if (cacheEnabled) {
      executor = new CachingExecutor(executor);
    }
	//插件包装
    executor = (Executor) interceptorChain.pluginAll(executor);
    return executor;
  }

public class CachingExecutor implements Executor {
  //执行器代理对象
  private final Executor delegate;
  //事务相关缓存,在commit时候会清空缓存,没有限制大小
  private final TransactionalCacheManager tcm = new TransactionalCacheManager();

  //查询实现
  public <E> List<E> query(MappedStatement ms, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql)
      throws SQLException {
	//获取缓存,根据MappedStatement传的是否有配置缓存,可以为每条SQL设置缓存与是否刷新缓存
    Cache cache = ms.getCache();
    if (cache != null) {
      flushCacheIfRequired(ms);
      if (ms.isUseCache() && resultHandler == null) {
        ensureNoOutParams(ms, boundSql);
        @SuppressWarnings("unchecked")
		//从缓存中获取数据
        List<E> list = (List<E>) tcm.getObject(cache, key);
		//查询不到的时候委托真正的Executor查询,查询到结果后放入缓存中
        if (list == null) {
          list = delegate.query(ms, parameterObject, rowBounds, resultHandler, key, boundSql);
          tcm.putObject(cache, key, list); // issue #578 and #116
        }
        return list;
      }
    }
	//没有设置缓存,真正的Executor查询
    return delegate.query(ms, parameterObject, rowBounds, resultHandler, key, boundSql);
  }
}
```

各种缓存实现:
- SynchronizedCache： 同步Cache，实现比较简单，直接使用synchronized修饰方法。
- LoggingCache： 日志功能，装饰类，用于记录缓存的命中率，如果开启了DEBUG模式，则会输出命中率日志。
- SerializedCache： 序列化功能，将值序列化后存到缓存中。该功能用于缓存返回一份实例的Copy，用于保存线程安全。
- LruCache： 采用了Lru算法的Cache实现，移除最近最少使用的key/value。
- PerpetualCache： 作为为最基础的缓存类，底层实现比较简单，直接使用了HashMap。


####一级缓存
BaseExecutor 是Executor的一个基本实现,有3个子类继承BaseExecutor,用于不同的功能
SimpleExecutor是一种常规执行器，每次执行都会创建一个statement，用完后关闭。
ReuseExecutor是可重用执行器，将statement存入map中，操作map中的statement而不会重复创建statement。
BatchExecutor是批处理型执行器，doUpdate预处理存储过程或批处理操作，doQuery提交并执行过程。
一级缓存实现
```java
  //BatchExecutor的一个缓存成员变量
  //查询的时候优先查询本地缓存,获取到缓存则返回,没有缓存则从DB查询,然后加入缓存
  //执行update时候会清空本地缓存
  protected PerpetualCache localCache;

  @Override
  public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException {
    ErrorContext.instance().resource(ms.getResource()).activity("executing a query").object(ms.getId());
    if (closed) {
      throw new ExecutorException("Executor was closed.");
    }
	// 清楚本地缓存
    if (queryStack == 0 && ms.isFlushCacheRequired()) {
      clearLocalCache();
    }
    List<E> list;
    try {
	  //从缓存中获取查询数据
      queryStack++;
      list = resultHandler == null ? (List<E>) localCache.getObject(key) : null;
      if (list != null) {
        handleLocallyCachedOutputParameters(ms, key, parameter, boundSql);
      } else {
		//缓存获取不到数据的情况下,委托子类从数据库查询,并将结果缓存到localCache中
        list = queryFromDatabase(ms, parameter, rowBounds, resultHandler, key, boundSql);
      }
    } finally {
      queryStack--;
    }
    if (queryStack == 0) {
      for (DeferredLoad deferredLoad : deferredLoads) {
        deferredLoad.load();
      }
      // issue #601
      deferredLoads.clear();
	  //如果是STATEMENT级别的缓存,清空本地缓存
      if (configuration.getLocalCacheScope() == LocalCacheScope.STATEMENT) {
        // issue #482
        clearLocalCache();
      }
    }
    return list;
  }
```

###整合spring
SqlSessionInterceptor调用SqlSessionUtils.getSqlSession时,如果没有不是事务的话,每次都会返回一个新的SqlSession,执行完成之后如果没有事务的话,会close掉SqlSession,因此一级缓存在spring中不起效,二级缓存如果在mapper中没有指定的话,也不起效.
在分布式系统中,由于mybatis使用的是本地缓存,有可能会出现脏数据情况,因此最好不使用本地缓存,可以使用集中式缓存如memcache或redis.

