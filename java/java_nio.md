###NIO几个概念:
1. Channel(通道):双向通道,可读可写,主要的实现有FileChannel(文件读写channel),DatagramChannel(UDP读写channel),SocketChannel(TCP客户端),ServerSocketChannel(TCP服务端),读写操作对象为Buffer
2. Buffer(缓冲区):channel读写数据区域,常用的有ByteBuffer, CharBuffer, DoubleBuffer, FloatBuffer, IntBuffer, LongBuffer, ShortBuffer对应相应的基础类型,还有每个类型下的HeapByteBuffer(堆上分配), DirectByteBuffer(直接内存分配)
3. Selector:Channel选择器,可以同时监听多个channel,返回活动的channel,如channel有新的连接,可读,可写等事件.

###Buffer
Buffer抽象类数据结构,以及常用的方法
```java
public abstract class Buffer {
    // Invariants 几个变量的关系: mark <= position <= limit <= capacity
	// 标记某次读或写的位置
	// 调mark()方法记录position
	// reset()方法还原position
    private int mark = -1;
	//当前读写的位置
    private int position = 0;
	//可读或写的上限
    private int limit;
	//buffer最大容量
    private int capacity;
	
	//分配直接内存是保存的内存地址
    long address;

	//清空Buffer,实际上并未删除Buffer中数据,将position置0,limit=capacity,清除mark标志,重写时原数据才会被覆盖
	public final Buffer clear() {
        position = 0;
        limit = capacity;
        mark = -1;
        return this;
    }

	// limit保存position位置,position置0,清除mark标志,写完后开始读取
    public final Buffer flip() {
        limit = position;
        position = 0;
        mark = -1;
        return this;
    }
	
	//是否还有剩余数据
    public final boolean hasRemaining() {
        return position < limit;
    }

	// 标记当前position
    public final Buffer mark() {
        mark = position;
        return this;
    }

	// 剩余元素
    public final int remaining() {
        return limit - position;
    }
	
	// 重置position,值为之前保存的mark值,回退到之前保存的position位置
    public final Buffer reset() {
        int m = mark;
        if (m < 0)
            throw new InvalidMarkException();
        position = m;
        return this;
    }

	//重置position,不改变limit的值,重置mark
    public final Buffer rewind() {
        position = 0;
        mark = -1;
        return this;
    }
}
```
其下各子类类似,有各自的类型处理,如抽象类ByteBuffer有成员变量byte[] hb,LongBuffer有long[] hb,成员变量仅对heap buffers的实现有用,direct buffer通过unsafe类操作内存.
以ByteBuffer为例,继承Buffer抽象类
```java
public abstract class ByteBuffer
    extends Buffer
    implements Comparable<ByteBuffer>{
	// 堆内存(heap buffers)分配时保存的数组
	final byte[] hb;
	// 数组偏移量,也就是数组中起始位置
    final int offset;
	// 堆内存(heap buffers)分配时只读标志
    boolean isReadOnly;

	// 分配堆内存,HeapByteBuffer构造函数没有修饰符,其他包的类不能调用HeapByteBuffer构造函数申请HeapByteBuffer
	public static ByteBuffer allocate(int capacity) {
        if (capacity < 0)
            throw new IllegalArgumentException();
        return new HeapByteBuffer(capacity, capacity);
    }
	// 分配堆外内存,也是通过此方法分配
    public static ByteBuffer allocateDirect(int capacity) {
        return new DirectByteBuffer(capacity);
    }
	
	// 将字节数组放到HeapByteBuffer缓存
    public static ByteBuffer wrap(byte[] array,
                                    int offset, int length)
    {
        try {
            return new HeapByteBuffer(array, offset, length);
        } catch (IllegalArgumentException x) {
            throw new IndexOutOfBoundsException();
        }
    }

	// 将区间[position,limit]之间的数据复制至 [0,limit-position]
	// position=limit-position,也就是新缓冲的最后位置
	// limit=capacity
	// mark=-1 失效
	public abstract ByteBuffer compact();
	
	// 缓冲复制,共享原数据,新缓冲修改会影响原缓冲,位置(position,limit,mark独立)
	public abstract ByteBuffer duplicate();

	// 缓冲区写数据 put,putInt,putLong等,往缓冲区写入数据,并移动相应的位置,如byte则position+1,long则position+8
	ByteBuffer putXXX(byte;int;long等);

	// put,putInt,putLong等,指定位置往缓冲区写入数据,不改变position位置
	ByteBuffer putXXX(int i,byte;int;long等);

	// 缓冲区读数据 get,getInt,getLong等,从缓冲区读取数据,并移动相应的位置,如byte则position+1,long则position+8
	char;int;long getXXX();

	// get,getInt,getLong等,指定位置从缓冲区读取数据,不改变position位置
	getXXX(int i);
	
	// 生成子缓冲,与原缓冲共享同一个数据,对其修改会反映到原缓冲,通过设置position与limit来限制子缓冲的起始与终止位置
    public abstract ByteBuffer slice();

}
```

####直接内存DirectByteBuffer释放时机
直接内存不是堆内存,不属于GC管辖范围
DirectByteBuffer是属于java堆对象,在gc回收后相应的堆外内存也会被回收掉(回收机制为sun.misc.Cleaner是虚引用(PhantomReference),在gc会收到通知,触发执行释放逻辑)
也可以直接显式调用 DirectByteBuffer.cleaner().clean()回收

执行释放代码位于父类java.lang.ref.Reference<T>,该类static代码块启动一个守护线程,如果是cleaner实例的话会调用其clean方法,也就是释放直接内存方法

###Channel
java.nio.channels.Channel表示一个实体的连接,像硬件,文件,网络socket等,
isOpen 返回channel是否开启状态
close 关闭channel,关闭后再操作channel会throw ClosedChannelException,幂等调用.

主要的类图:
![Channel类图](pic/nio_Channel.png)


Channel:顶层接口,表示一个IO操作,提供判断是否打开与关闭接口
WritableByteChannel:继承Channel接口,提供把缓冲区Buffer内容写入Channel方法
ReadableByteChannel:继承Channel接口,提供从Channel读取数据到Buffer方法
NetworkChannel: 继承Channel接口,网络socket的Channel,提供socket操作相关方法,如绑定网络地址bind方法
InterruptibleChannel: 可中断的Channel,如果一个线程在阻塞等待数据,另一个线程调用close方法,则阻塞的线程会抛出AsynchronousCloseException异常
GatheringByteChannel:继承WritableByteChannel,给定的数组Buffer[],按顺序写入channel
ScatteringByteChannel,继承ReadableByteChannel,
ByteChannel:继承WritableByteChannel,ReadableByteChannel,可读写的字节Channel,自身没有定义其他方法
MulticastChannel,继承NetworkChannel,提供组播操作,加入一个组
SeekableByteChannel,继承ByteChannel,提供变长字节操作功能,通过position获取或操作指定位置.

还有异步的类asynchronous..Channel

####FileChannel
读写,映射,操作文件
```java
public abstract class FileChannel
    extends AbstractInterruptibleChannel
    implements SeekableByteChannel, GatheringByteChannel, ScatteringByteChannel{

	//打开一个文件,通过静态方法打开文件,OpenOption打开标识StandardOpenOption提供了文件操作枚举如read,write等
	public static FileChannel open(Path path, OpenOption... options) throws IOException{
		
	}
	// 刷新到存储设备,如磁盘等
	// metaData表示是否更新文件元信息(如权限等),true表示更新文件内容与更新文件元信息,false表示只更新文件内容
    public abstract void force(boolean metaData) throws IOException;
	
	// 锁定文件区域,position起始位置,size长度,share是否可共享
    public abstract FileLock lock(long position, long size, boolean shared)
        throws IOException;

	// 映射文件到虚拟内存
	// MapMode标识可读或可写,或私有private(可读可写,但不会影响文件内容)
	// position文件起始位置
	// 
    public abstract MappedByteBuffer map(MapMode mode,
                                         long position, long size)
        throws IOException;

	// read系列方法,读取文件内容到指定buffer
	
	// 从指定可读channel,起始位置position,指定大小count,读取到当前fileChannel
	public abstract long transferFrom(ReadableByteChannel src,
                                      long position, long count)
        throws IOException;
	
	//从当前fileChannel起始位置position,大小count,写到target去
    public abstract long transferTo(long position, long count,
                                    WritableByteChannel target)
        throws IOException;

	// 保留指定大小的文件, 如果size>=当前文件大小,相当于没修改
    public abstract FileChannel truncate(long size) throws IOException;

	//尝试获取锁,如果没有获取到锁,返回null
    public abstract FileLock tryLock(long position, long size, boolean shared)
        throws IOException;

	// write系列,给定的buffer写入到fileChannel中去
}
```

map方法与传统read方法比较
read需进行系统调用,需将文件内容从磁盘复制到内核空间,再从内核空间复制到jvm堆内存中,中间经过两次复制
map也是系统调用,直接把文件当成虚拟内存,当需读取文件时发生缺页中断在从磁盘读取文件,直接从磁盘复制到用户空间

transferTo,transferFrom可以减少传统read,write方法的内存复制次数,传统read需将内容从 硬件设备->内核空间->用户空间,write放向 用户空间->内核空间->另一个硬件设备, transferTo,transferFrom可以实现从硬件设备->内核空间->另一个硬件设备.

###AbstractSelectableChannel
表示一个channel是可以注册到selector,是DatagramChannel,ServerSocketChannel,SocketChannel的父类
```java
    // 设置channel是否阻塞
	public final SelectableChannel configureBlocking(boolean block)
        throws IOException
    {...}

	// 将自身注册到一个Selector
	// 如果已经参数sel存在SelectionKey(包装了SelectableChannel与selector关系),则修改ops与att
	// 如果不存在则调用 (AbstractSelector)sel.register将自身(channel)注册到Selector中去
	// ops 监听感兴趣事件的集合
	// SelectionKey定义了OP_ACCEPT,OP_CONNECT,OP_READ,OP_WRITE,多种事件可以 or连接 
	// 如(OP_READ|OP_WRITE 表示监听channel可读或可写收到通知)
	// att 可以保存自定义数据到SelectionKey
    public final SelectionKey register(Selector sel, int ops,
                                       Object att)
        throws ClosedChannelException
    {....}
```


###ServerSocketChannel
ServerSocketChannel是面向连接TCP的server端实现,可以监听新进来的TCP连接,其继承了AbstractSelectableChannel,表示此channel可以注册到selector监听.

```java
public abstract class ServerSocketChannel
    extends AbstractSelectableChannel
    implements NetworkChannel
{
	// 打开一个ServerSocketChannel,在accept之前需绑定bind本地地址
    public static ServerSocketChannel open() throws IOException {
        return SelectorProvider.provider().openServerSocketChannel();
    }

	// 接受一个连接,阻塞至有新连接进来或I/O error,如果设置成nonblocking,会立刻返回null,如果有新连接,返回一个连接SocketChannel.
	public abstract SocketChannel accept() throws IOException;

	// 绑定一个地址,backlog表示预置最大连接数,如果是0或负数则表示用系统默认值
    public abstract ServerSocketChannel bind(SocketAddress local, int backlog)
        throws IOException;

	// 返回channel关联的socket对象
    public abstract Socket socket();
}
```

###SocketChannel
socketChannel用于连接socket,表示一个实体连接(TCP),连接成功后(客户端调用connect,服务端accept之后会在服务端生成一个SocketChannel 表示连接成功),可对channel进行读写操作,同样继承了AbstractSelectableChannel,可注册到selector监听,同时也实现了字节流操作接口ByteChannel, ScatteringByteChannel, GatheringByteChannel.
```java
public abstract class SocketChannel
    extends AbstractSelectableChannel
    implements ByteChannel, ScatteringByteChannel, GatheringByteChannel, NetworkChannel
{
	// 开启一个连接SocketChannel
    public static SocketChannel open() throws IOException

	// 开启一个连接SocketChannel,并连接到remote地址,就是无参数open+connect
	public static SocketChannel open(SocketAddress remote) throws IOException

	//绑定本地指定地址/端口
    public abstract SocketChannel bind(SocketAddress local) throws IOException;

	// 连接到远程机器
	// 非阻塞non-blocking模式下,如果能立即连接成功则返回true,如果不能则返回false,后续调用finishConnect来完成连接
	// 阻塞blocking模式下,连接成功返回true,否则进入阻塞状态,直到连接成功或出现IOException
    public abstract boolean connect(SocketAddress remote) throws IOException;
	
	// 完成连接
	// 如果已经是连接成功则立即返回true
    // 非阻塞non-blocking模式下通过调用connect开始,finishConnect完成
	// 如果连接成功返回true,如果还未连接成功返回false
	public abstract boolean finishConnect() throws IOException;
	
	// read方法,从Channel读取字节流到ByteBuffer,未连接成功则会抛出NotYetConnectedException
	public abstract int read(ByteBuffer dst)

	// 关闭输入流但不关闭socket连接,停止接收数据
	// 如果执行过此方法,后续读取channel数据时返回-1
	// 如果socket连接已经关闭,则调用此方法将无效果
	// 如果socket没有连接成功则抛出NotYetConnectedException
    public abstract SocketChannel shutdownInput();

	// 关闭输入出但不关闭socket连接,停止接收数据
	// 如果执行过此方法,后续往channel写数据时抛出ClosedChannelException
	// 如果socket连接已经关闭,则调用此方法将无效果
	public abstract SocketChannel shutdownOutput()

	// 将缓冲区ByteBuffer数据写入channel
	public abstract int write(ByteBuffer src) 

	// 返回channel关联的socket对象
    public abstract Socket socket();

}
```

###DatagramChannel
DatagramChannel面向报文数据传输(UDP)channel,可以不需要创建真正的连接,通过send,receive发送与接收数据
```java
public abstract class DatagramChannel
    extends AbstractSelectableChannel
    implements ByteChannel, ScatteringByteChannel, GatheringByteChannel, MulticastChannel
{
	// 开启一个UDP连接,但不会真正连接到服务器,ipv6可用的话使用ipv6,否则使用ipv4
	public static DatagramChannel open() throws IOException 

	// 指定协议簇ipv4或ipv6,枚举值StandardProtocolFamily
    public static DatagramChannel open(ProtocolFamily family) throws IOException 

	// 绑定网络地址,ip端口
    public abstract DatagramChannel bind(SocketAddress local)throws IOException;

	// 连接到远程网络地址,一旦连接成功后,当前DatagramChannel只能往目的地址发送与接收数据
    public abstract DatagramChannel connect(SocketAddress remote) throws IOException;

	// 取消连接,如果在读取或写入数据时候调用,将会等待readLock与writeLock
	// 如果没有发起连接或被close,也不会有影响
    public abstract DatagramChannel disconnect() throws IOException;

	// 判断channel是否有连接
    public abstract boolean isConnected();

	// 连接成功后读取channel的数据到ByteBuffer,如果数据报文超过ByteBuffer大小,超过部分会被丢弃
	// 没有连接成功情况下调用read会抛出NotYetConnectedException
    public abstract int read(ByteBuffer dst) throws IOException;

	// 连接成功后将ByteBuffer数据写入channel
	// 没有连接成功情况下调用read会抛出NotYetConnectedException
    public abstract int write(ByteBuffer src) throws IOException;

	// 接收数据并写入ByteBuffer,如果ByteBuffer剩余容量不足以保存报文,则报文会被截断,没有必须连接要求,返回发送端的地址信息
	// 阻塞channel:如果没有消息,线程会被阻塞直到有消息
	// 非阻塞Channel:如果没有消息立即返回null
    public abstract SocketAddress receive(ByteBuffer dst) throws IOException;

	// 发送数据报文到指定地址
	// 如果已经连接成功且target不是连接的对方,则抛出IllegalArgumentException
    public abstract int send(ByteBuffer src, SocketAddress target)
        throws IOException;

	// 返回socket对象
    public abstract DatagramSocket socket();

}
```

####Selector
Selector为多路复用选择器,用于检查一个或多个SelectableChannel的读写状态,可以实现单线程管理多个channel,当多个channel读写频率较少时,相比传统阻塞IO,可以减少线程创建/切换

```java
public abstract class Selector implements Closeable {
	// 开启一个channel选择器,SelectorProvider由操作系统决定
	// 如Windows的poll(WindowsSelectorImpl)
	// Linux的epoll(EPollSelectorProvider)
    public static Selector open() throws IOException 
	
	// 关闭一个selector
	// 如果有另一个线程阻塞在select或select(long)方法,close之前先调用wakeup()唤醒阻塞的线程
	// 所有关联此selector的channels将会取消关联关系
	// 关闭之后再操作此selector将会抛出ClosedSelectorException
    public abstract void close() throws IOException;
	
	// 测试selector是否开启状态
    public abstract boolean isOpen();

	// 返回注册在此selector上所有的SelectionKey(包括selector与channel),不能修改Set
    public abstract Set<SelectionKey> keys();

	// 选择注册的channel中,返回已准备好(变化)的key个数,如果之前selectKey没有移除且key准备好,可能返回0但selectKeys不为空
	// 调这方法的线程会阻塞,至少满足以下条件才返回:
	// 1. 至最少有一个channel准备好
	// 2. 该selector的wakeup方法被调用
	// 3. 阻塞的线程被中断interrupted
	// select时候锁住了selector对象,多线程调用此方法时会阻塞
    public abstract int select() throws IOException;

	// 与select一样
	// 参数超时timeout为毫秒milliseconds,0表示无限等待(与不带参数select一样),不能为负数
	// 在超时时间内有准备好的channel,则返回准备好的channel个数,到超时时间都没有准备好,则返回0
    public abstract int select(long timeout)
        throws IOException;

	// 返回准备好的channel对应的key集合,集合不是线程安全的
	// 集合只能删除,不能添加
    public abstract Set<SelectionKey> selectedKeys();

	// 非阻塞select,如果没有准备好的channel则返回0
	// 此方法会清除wakeup方法设置的唤醒标识
    public abstract int selectNow() throws IOException;

	// 唤醒一个正在等待select IO返回的线程,并不会唤醒由于同步阻塞的线程
	public abstract Selector wakeup();
}
```

###AbstractSelector
默认实现的selector抽象类,在selector接口定义的方法上增加了如
取消cancelledKeys方法,注册register,取消注册deregister
在执行IO操作,如select方法时候,先调begin方法注册中断处理函数,IO返回后调用end解除中断处理.

```java
public abstract class AbstractSelector
    extends Selector
{	
	// 保存取消的key,select执行IO之前将此集合的key全部解除注册,不再监听
	private final Set<SelectionKey> cancelledKeys = new HashSet<SelectionKey>();
	
	//中断处理接口
	private Interruptible interruptor = null;

	// 执行IO操作前,注册一个中断函数,函数执行体为调用 wakeup
	protected final void begin() 

	// 执行IO(select)操作之后清空中断函数操作
    protected final void end() 

	// 注册一个channel到当前selector,ops为SelectionKey定义的常量, 可以用or | 操作注册多个事件,att为关联的字段
    protected abstract SelectionKey register(AbstractSelectableChannel ch,
                                             int ops, Object att);

	// 解除注册
    protected final void deregister(AbstractSelectionKey key) 

}
```

####SelectionKey
SelectionKey数据结构维护channel与selector间关系,定义事件常量值
```java
public abstract class SelectionKey {
	// 可读事件
	public static final int OP_READ = 1 << 0;
	// 可写事件
    public static final int OP_WRITE = 1 << 2;
	// 连接事件
    public static final int OP_CONNECT = 1 << 3;
    // 新连接建立事件
	public static final int OP_ACCEPT = 1 << 4;

	// 取消selector中监控事件,不再监听此key事件
	// 加入 AbstractSelector.cancelledKeys集合中
    public abstract void cancel();

	// 返回key关联的SelectableChannel
    public abstract SelectableChannel channel();

	// 返回key关联的selector
    public abstract Selector selector();

	// 返回注册的事件集合
    public abstract int interestOps();

	// 设置事件集合
    public abstract SelectionKey interestOps(int ops);

	// 返回当前channel准备好的事件集合
    public abstract int readyOps();

}
```


###Linux的nio:epoll

####Linux下的Epoll函数
Linux下主要通过一下3个函数操作epoll
//创建Epoll
extern int epoll_create (int \__size) \__THROW;
//维护Epoll集合元素
extern int epoll_ctl (int \__epfd, int \__op, int \__fd,struct epoll_event \*\__event) \__THROW;
//等待准备好的fd事件
extern int epoll_wait (int \__epfd, struct epoll_event \*\__events,int \__maxevents, int \__timeout);
```C
//include epoll头文件
#include<sys/epoll.h>

// 自定义数据联合体
typedef union epoll_data
{
  void *ptr;
  int fd;
  uint32_t u32;
  uint64_t u64;
} epoll_data_t;

//epoll结构体,events表示监听的事件集合,宏定义常量,用or | 表示多个事件
struct epoll_event
{
  uint32_t events;      /* Epoll events */
  epoll_data_t data;    /* User data variable */
} 

//events可以是以下几个宏的集合：
//EPOLLIN ：表示对应的文件描述符可以读（包括对端SOCKET正常关闭）；
//EPOLLOUT：表示对应的文件描述符可以写；
//EPOLLPRI：表示对应的文件描述符有紧急的数据可读（这里应该表示有带外数据到来）；
//EPOLLERR：表示对应的文件描述符发生错误；
//EPOLLHUP：表示对应的文件描述符被挂断；
//EPOLLET： 将EPOLL设为边缘触发(Edge Triggered)模式，这是相对于水平触发(Level Triggered)来说的。
//EPOLLONESHOT：只监听一次事件，当监听完这次事件之后，如果还需要继续监听这个socket的话，需要再次把这个socket加入到EPOLL队列里


// fd: file descriptors,Linux系统维护的文件描述符,是个正整数
// 创建一个epoll,返回epoll的fd,size表示epoll fd初始对应的关联其他fd的数量,使用完成后需close掉
extern int epoll_create (int __size) __THROW;

// 维护epoll的文件描述符
// __epfd,由epoll_create返回的fd,
// __op,操作类型,定义字符常量EPOLL_CTL_ADD(添加),EPOLL_CTL_DEL(删除),EPOLL_CTL_MOD(修改)
// __fd,需要交由epfd监控的目标fd
// __event结构体
// 返回0表示成功,-1表示失败并设置errno
extern int epoll_ctl (int __epfd, int __op, int __fd,
                      struct epoll_event *__event) __THROW;

// 阻塞至事件准备好,跟Java中select一样,返回准备好的事件数目
// __epfd,由epoll_create返回的fd
// __events准备好的事件集合
// __maxevents,__events集合大小
// __timeout等待时间, 
//     -1 表示一直等待直到有事件准备好
//	    0 立刻返回
//	    正整数 等待的毫秒数
extern int epoll_wait (int __epfd, struct epoll_event *__events,
                       int __maxevents, int __timeout);
```
监听事件中,EPOLLET设置边缘触发(Edge Triggered)模式，这是相对于水平触发(Level Triggered)来说的。
ET: 需指定,对于每一个被通知的文件描述符，如可读，则必须将该文件描述符一直读到空，让 errno 返回 EAGAIN 为止，否则下次的 epoll_wait 不会返回余下的数据，会丢掉事件.
LT: 默认设置,只要这个文件描述符还有数据可读，每次 epoll_wait都会返回它的事件，提醒用户程序去操作

####Linux Epoll实现机制
在调用epoll_create函数时候,内核会创建一个红黑树rbn与双向链表rdllist,当调epoll_ctl时,实际上是维护红黑树rbn,如EPOLL_CTL_ADD操作往红黑树插入节点,EPOLL_CTL_DEL表示删除红黑树节点,EPOLL_CTL_MOD表示修改红黑树节点
所有添加到epoll中的事件都会与设备(如网卡)驱动程序建立回调关系,相应事件触发时会执行回调将事件放到链表rdllist中.
在epoll_wait阻塞时候只需检查rdllist是否为空即可,如果不为空则将链表复制至用户空间(user space),返回事件个数.
参考代码: linux-2.6.32.6/fs/eventpoll.c

###字节流与数据包
TCP协议是面向流的传输协议,不是数据包,我们传输两个数据包在操作系统看起来是一串字节流,TCP传输受MTU(Maximum Transmission Unit)影响(常用1500),假设一个报文长度是1600,另一个报文长度400,到TCP传输时候也分成两段,分别是1500与500(第一次传了第一个报文的1500,第二次传了第一个报文的100与第二个报文的400)
解决:
1. 在报文开头增加报文字节长度,读取时候按字节长度读取
2. 设置数据包的长度为固定的长度，不够数据则以空格填补
3. 应用层在发送每个数据包时，给每个数据包加分界标记

UCP是基于报文发送的，UDP报文的首部会有16bit来表现UDP数据的长度，所以不同的报文之间是可以区别隔离出来的，所以应用层接收传输层的报文时，不会存在拆包和粘包的问题


###Pipe管道
java.nio.channels.Pipe是单向数据传输管道,其中
SourceChannel表示读取管道
SinkChannel表示写入管道
两个管道都继承了AbstractSelectableChannel
```java
public abstract class Pipe {
	//开启一个管道
	public static Pipe open() throws IOException
	//返回写入管道
	public abstract SinkChannel sink();
	//返回读取管道
	public abstract SourceChannel source();
}
```

###示例
####EchoServer实现
将用户输入字符回显
```java

public class EchoServer {

	// 绑定地址与端口
	private static int port = 1111;
	private static String hostName = "127.0.0.1";

	public static void main(String[] args) throws IOException {
		// 开启一个serverSocketChannel绑定地址
		ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
		serverSocketChannel.bind(new InetSocketAddress(hostName, port));
		//channel设置成非阻塞,注册到selector必须是非阻塞的
		serverSocketChannel.configureBlocking(false);

		//开启一个selector,serverSocketChannel注册ACCEPT事件到selector
		Selector selector = Selector.open();
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

		while (true) {
			// 选择准备好的事件,阻塞方法
			int selectCount = selector.select();
			if (selectCount <= 0) {
				continue;
			}
			// 获取所有准备好的key
			Set<SelectionKey> selectedKeys = selector.selectedKeys();
			Iterator<SelectionKey> iterator = selectedKeys.iterator();
			while (iterator.hasNext()) {
				SelectionKey selectionKey = iterator.next();
				iterator.remove();
				handleKey(selectionKey, serverSocketChannel, selector);
			}
		}
	}

	// 处理准备好的事件
	private static void handleKey(SelectionKey selectionKey, ServerSocketChannel serverSocketChannel, Selector selector) {
		try {
			// 处理新接入的连接
			if (selectionKey.isAcceptable()) {
				// accept返回一个新的socket连接,表示与客户端的连接
				SocketChannel socketChannel = serverSocketChannel.accept();
				System.out.println("a new connection from : " + socketChannel.getRemoteAddress());
				// 注册读取事件到selector,attachment为Buffer缓冲
				// 注册到selector必须是非阻塞的
				socketChannel.configureBlocking(false);
				socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(1024));
			}

			// 可读,客户端发送数据
			if (selectionKey.isReadable()) {
				ByteBuffer buffer = (ByteBuffer) selectionKey.attachment();
				buffer.clear();
				SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
				// 将字节流写到buffer,read返回-1表示结束
				int read = socketChannel.read(buffer);
				if (read < 0) {
					close(selectionKey);
					return;
				}
				// 读取字节流到buffer并回写到channel传回给客户端
				buffer.flip();
				System.out.println(String.format("server receive message from %s,content:%s", socketChannel.getRemoteAddress(), new String(buffer.array(), buffer.position(), buffer.limit())));
				socketChannel.write(buffer);
			}

			// 处理可写事件
			if (selectionKey.isWritable()) {

			}
		} catch (Exception e) {
			e.printStackTrace();
			close(selectionKey);
		}
	}
	
	// 关闭连接
	private static void close(SelectionKey selectionKey) {
		selectionKey.cancel();
		try {
			selectionKey.channel().close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
```

####EchoClient客户端:
```java

public class EchoClient {
	// 服务端地址与端口
	private static int port = 1111;
	private static String hostName = "127.0.0.1";

	// 连接关闭标识
	private static volatile boolean channelClosed = false;
	// 同步lock
	private static Object closeLock = new Object();

	public static void main(String[] args) throws IOException {
		// 开启一个socket,连接到服务端
		SocketChannel socketChannel = SocketChannel.open();
		boolean connect = socketChannel.connect(new InetSocketAddress(hostName, port));
		if (!connect) {
			System.err.println("connect to host " + hostName + ":" + port + " fail");
			return;
		}
		// 接收消息线程
		Thread receiveMsgThread = new Thread(() -> receiveMsg(socketChannel));
		receiveMsgThread.start();
		// 发送消息线程
		Thread sendMsgThread = new Thread(() -> sendMsg(socketChannel));
		sendMsgThread.start();
	
		// 等待连接关闭
		synchronized (closeLock) {
			while (!channelClosed) {
				try {
					closeLock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		//中断读写线程
		receiveMsgThread.interrupt();
		sendMsgThread.interrupt();
		socketChannel.close();
	}

	// 接收消息
	private static void receiveMsg(SocketChannel socketChannel) {
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		try {
			// read返回-1表示读取结束,关闭连接
			int readCount = -1;
			while (!channelClosed) {
				readCount = socketChannel.read(buffer);
				if (readCount < 0) {
					channelClosePending(socketChannel);
					return;
				}
				// 切换到读取模式
				buffer.flip();
				System.out.println("receive msg:" + new String(buffer.array(), buffer.position(), buffer.limit()));
				buffer.clear();
			}
		} catch (Exception e) {
			channelClosePending(socketChannel);
			e.printStackTrace();
		}
	}

	//从标准输入读取字符发送到服务端,输入bye结束
	private static void sendMsg(SocketChannel socketChannel) {
		ByteBuffer src = ByteBuffer.allocate(1024);
		String content = "";
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));) {
			content = br.readLine();
			while (!"bye".equals(content) && !channelClosed) {
				src.clear();
				src.put(content.getBytes());
				src.flip();
				socketChannel.write(src);
				content = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		channelClosePending(socketChannel);
	}

	// 设置关闭线程,唤醒主线程
	private static void channelClosePending(SocketChannel socketChannel) {
		if (!socketChannel.isOpen()) {
			return;
		}
		channelClosed = true;
		synchronized (closeLock) {
			closeLock.notifyAll();
		}
	}
}
```

