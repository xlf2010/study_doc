##SpringApplication
基于spring boot2.1.6来
Spring boot 启动

```java

@SpringBootApplication	//标记是个spring boot应用程序
public class SpringApplicationMain {
	public static void main(String[] args) {
    	//调用SpringApplication提供的静态方法
		SpringApplication.run(SpringApplicationMain.class, args);
	}
}

//SpringBootApplication注解，主要由以下3个组合而成
@SpringBootConfiguration	//启用扫描配置@Configuration注解的类
@EnableAutoConfiguration	//启用自动发现配置，可以自定义排除的类
@ComponentScan(excludeFilters = {
		@Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
		@Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })	//自动扫描组件
public @interface SpringBootApplication{

}

```
SpringApplication.java
````java
	public static ConfigurableApplicationContext run(Class<?>[] primarySources, String[] args) {
		return new SpringApplication(primarySources).run(args);
	}

    //new SpringApplication(Object[]) 会调用initialize方法
	public SpringApplication(ResourceLoader resourceLoader, Class<?>... primarySources) {
		this.resourceLoader = resourceLoader;
		Assert.notNull(primarySources, "PrimarySources must not be null");
		this.primarySources = new LinkedHashSet<>(Arrays.asList(primarySources));
		this.webApplicationType = WebApplicationType.deduceFromClasspath();
		//获取上下文初始化处理类
		setInitializers((Collection) getSpringFactoriesInstances(ApplicationContextInitializer.class));
		//获取ApplicationListener，用于监听spring应用启动的消息
		//通过EventPublishingRunListener推送应用启动/环境准备等消息
		setListeners((Collection) getSpringFactoriesInstances(ApplicationListener.class));
		this.mainApplicationClass = deduceMainApplicationClass();
	}

	private static final String[] SERVLET_INDICATOR_CLASSES = { "javax.servlet.Servlet",
			"org.springframework.web.context.ConfigurableWebApplicationContext" };

	private static final String WEBMVC_INDICATOR_CLASS = "org.springframework." + "web.servlet.DispatcherServlet";

	private static final String WEBFLUX_INDICATOR_CLASS = "org." + "springframework.web.reactive.DispatcherHandler";

	private static final String JERSEY_INDICATOR_CLASS = "org.glassfish.jersey.servlet.ServletContainer";

	//判断当前执行的环境的分三种，REACTIVE，SERVLET，NONE
	static WebApplicationType deduceFromClasspath() {
		if (ClassUtils.isPresent(WEBFLUX_INDICATOR_CLASS, null) && !ClassUtils.isPresent(WEBMVC_INDICATOR_CLASS, null)
				&& !ClassUtils.isPresent(JERSEY_INDICATOR_CLASS, null)) {
			return WebApplicationType.REACTIVE;
		}
		for (String className : SERVLET_INDICATOR_CLASSES) {
			if (!ClassUtils.isPresent(className, null)) {
				return WebApplicationType.NONE;
			}
		}
		return WebApplicationType.SERVLET;
	}
````
initialize方法主要作用是：
1. 保存source类
2. 判断是否web环境，判断依据是当前类路径是否包含 "javax.servlet.Servlet",			"org.springframework.web.context.ConfigurableWebApplicationContext"  这两个类，如果当前的classLoader都能加载到，则认为是web环境
3. 初始化实现了接口ApplicationContextInitializer并且配置在META-INF/spring.factories上，通过反射调用默认构造函数实例化，按照order定义的优先级排序(如果有注解org.springframework.core.annotation.Order的话)。

```java
public interface ApplicationContextInitializer<C extends ConfigurableApplicationContext> {
	//ConfigurableApplicationContext 可以实现此接口初始化ConfigurableApplicationContext
	void initialize(C applicationContext);

}
```
META-INF/spring.factories配置了几个初始化接口实现,在创建完成ConfigurableApplicationContext后会执行以下类的initialize方法, 我们可以在spring初始化时候实现此接口然后配置到spring.factories上实现初始化.
```properties
# spring boot包下的MATE-INF/spring.factories定义了
# Application Context Initializers
org.springframework.context.ApplicationContextInitializer=\
#warning ConfigurationWarningsPostProcessor
org.springframework.boot.context.ConfigurationWarningsApplicationContextInitializer,\
#contextId 
org.springframework.boot.context.ContextIdApplicationContextInitializer,\
org.springframework.boot.context.config.DelegatingApplicationContextInitializer,\
org.springframework.boot.web.context.ServerPortInfoApplicationContextInitializer
```

4. 初始化实现了接口ApplicationListener并且配置在META-INF/spring.factories上，通过反射调用默认构造函数实例化，按照order定义的优先级排序(如果有注解org.springframework.core.annotation.Order的话),

```java
@FunctionalInterface
public interface ApplicationListener<E extends ApplicationEvent> extends EventListener {
	// 处理ApplicationEvent事件,如在启动过程中会调用此方法处理ApplicationStartingEvent,ApplicationPreparedEvent等等处理启动过程的事件
	void onApplicationEvent(E event);
}
```

```properties
# spring boot包下的MATE-INF/spring.factories定义了
org.springframework.context.ApplicationListener=\
org.springframework.boot.ClearCachesApplicationListener,\
org.springframework.boot.builder.ParentContextCloserApplicationListener,\
org.springframework.boot.context.FileEncodingApplicationListener,\
org.springframework.boot.context.config.AnsiOutputApplicationListener,\
org.springframework.boot.context.config.ConfigFileApplicationListener,\
org.springframework.boot.context.config.DelegatingApplicationListener,\
org.springframework.boot.context.logging.ClasspathLoggingApplicationListener,\
org.springframework.boot.context.logging.LoggingApplicationListener,\
org.springframework.boot.liquibase.LiquibaseServiceLocatorApplicationListener
```

5. 查找main方法所在的类,通过当前的执行栈来确定。

来看getSpringFactoriesInstances是如何执行的
```java
	private <T> Collection<? extends T> getSpringFactoriesInstances(Class<T> type,
			Class<?>[] parameterTypes, Object... args) {
        //获取当前的classLoader
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		// Use names and ensure unique to protect against duplicates
        //根据配置文件获取class类名
		Set<String> names = new LinkedHashSet<String>(
				SpringFactoriesLoader.loadFactoryNames(type, classLoader));
        //实例化获取到的类名
		List<T> instances = createSpringFactoriesInstances(type, parameterTypes,
				classLoader, args, names);
        //如果有注解org.springframework.core.annotation.Order，按Order注解配置的顺序排序
		AnnotationAwareOrderComparator.sort(instances);
		return instances;
	}

	//创建在配置文件spring.factories中的所有类实例。
    private <T> List<T> createSpringFactoriesInstances(Class<T> type,
			Class<?>[] parameterTypes, ClassLoader classLoader, Object[] args,
			Set<String> names) {
		List<T> instances = new ArrayList<T>(names.size());
		for (String name : names) {
			try {
				Class<?> instanceClass = ClassUtils.forName(name, classLoader);
				Assert.isAssignable(type, instanceClass);
				Constructor<?> constructor = instanceClass
						.getDeclaredConstructor(parameterTypes);
				T instance = (T) BeanUtils.instantiateClass(constructor, args);
				instances.add(instance);
			}
			catch (Throwable ex) {
				throw new IllegalArgumentException(
						"Cannot instantiate " + type + " : " + name, ex);
			}
		}
		return instances;
	}
```

SpringFactoriesLoader类似java提供的SPI机制来实现，通过扫描spring.factories配置文件来查找相关的实现类，可实现不改动源代码，改动配置文件来实现功能的更新，需要代码面向接口编程。
```java
	public static List<String> loadFactoryNames(Class<?> factoryClass, ClassLoader classLoader) {
		String factoryClassName = factoryClass.getName();
		try {
        	//获取所有的spring.factories文件路径
			Enumeration<URL> urls = (classLoader != null ? classLoader.getResources(FACTORIES_RESOURCE_LOCATION) :
					ClassLoader.getSystemResources(FACTORIES_RESOURCE_LOCATION));
			List<String> result = new ArrayList<String>();
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				Properties properties = PropertiesLoaderUtils.loadProperties(new UrlResource(url));
                //读取配置，key为类的全名。
				String factoryClassNames = properties.getProperty(factoryClassName);
			//解析多个实现类，以,分割多个配置	result.addAll(Arrays.asList(StringUtils.commaDelimitedListToStringArray(factoryClassNames)));
			}
			return result;
		}
		catch (IOException ex) {
			throw new IllegalArgumentException("Unable to load [" + factoryClass.getName() +
					"] factories from location [" + FACTORIES_RESOURCE_LOCATION + "]", ex);
		}
	}
```

初始化完毕后将会执行org.springframework.boot.SpringApplication.run(String...)方法

1. 获取spring.factories,key=SpringApplicationRunListener的类并实例化
2. 调用所有SpringApplicationRunListener的starting方法
3. 准备环境,根据当前是否有对应的类来决定初始化的环境,可以是StandardServletEnvironment(web环境),StandardEnvironment等等.
4. 打印banner
5. 创建ConfigurableApplicationContext,根据当前环境的类来决定.
6. 准备上下文
7. 刷新上下文
8. 刷新之后的操作
9. 调用所有SpringApplicationRunListener的running方法

```java
public ConfigurableApplicationContext run(String... args) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		ConfigurableApplicationContext context = null;
		FailureAnalyzers analyzers = null;
		configureHeadlessProperty();
        //1. 从spring.factories获取配置key为org.springframework.boot.SpringApplicationRunListener的配置,spring boot包下定义了一个EventPublishingRunListener
        //org.springframework.boot.SpringApplicationRunListener=\
		//org.springframework.boot.context.event.EventPublishingRunListener
		SpringApplicationRunListeners listeners = getRunListeners(args);
        //调用listeners的starting方法
		listeners.starting();
		try {
			ApplicationArguments applicationArguments = new DefaultApplicationArguments(
					args);

            //准备环境
            //1.如果是WebApplicationType.SERVLET是的话会生成StandardServletEnvironment，
            //2.如果是WebApplicationType.REACTIVE否则生成StandardReactiveWebEnvironment,
            //3.否则生成StandardEnvironment
            //解析命令行传的参数args，调用全部listeners.environmentPrepared方法
			ConfigurableEnvironment environment = prepareEnvironment(listeners,
					applicationArguments);

            //是否打印banner，打印在jar包下的banner.txt文件，或者图片
			Banner printedBanner = printBanner(environment);

            //创建ConfigurableApplicationContext，
            //1.如果是WebApplicationType.SERVLET环境实现类为org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext
            //2.如果是WebApplicationType.REACTIVE否则生成org.springframework.boot.web.reactive.context.AnnotationConfigReactiveWebServerApplicationContext
            //3.否则实现类为org.springframework.context.annotation.AnnotationConfigApplicationContext
            //以上3个实现类会在同一个父类GenericApplicationContext初始化org.springframework.beans.factory.support.DefaultListableBeanFactory
			//构造函数初始化AnnotatedBeanDefinitionReader是会注册
			//ConfigurationClassPostProcessor配置处理类，
			//AutowiredAnnotationBeanPostProcessor处理，
			//CommonAnnotationBeanPostProcessor普通注解处理（jsr250开启），
			//PersistenceAnnotationBeanPostProcessor，jpa数据源处理(jpa开启)
			context = createApplicationContext();
            
			//创建异常分析类，实现类配置在spring.factories上,key=SpringBootExceptionReporter,里面的FailureAnalyzers是在spring.factories配置的key=org.springframework.boot.diagnostics.FailureAnalyzer
            /**
            # Error Reporters
            org.springframework.boot.SpringBootExceptionReporter=\
            org.springframework.boot.diagnostics.FailureAnalyzers
			*/
			analyzers = new FailureAnalyzers(context);
            //准备context上下文，加载初始化sources类
			prepareContext(context, environment, listeners, applicationArguments,
					printedBanner);
            //刷新上下文
			refreshContext(context);
			//刷新之后的操作,子类可以重写这个方法自定义操作
            afterRefresh(context, applicationArguments);
            //调用listeners的started方法,
            listeners.started(context);
            //获取实现了ApplicationRunner或CommandLineRunner的bean,如果定义了顺序,则按排序规则调用其run方法
			callRunners(context, applicationArguments);
			stopWatch.stop();
			if (this.logStartupInfo) {
				new StartupInfoLogger(this.mainApplicationClass)
						.logStarted(getApplicationLog(), stopWatch);
			}
			return context;
		}
		catch (Throwable ex) {
            //处理失败情况,广播ExitCodeEvent事件,调用listeners.failed(context, exception);
			handleRunFailure(context, ex, exceptionReporters, listeners);
			throw new IllegalStateException(ex);
		}

		try {
            //调用listeners的running方法,
			listeners.running(context);
		}
		catch (Throwable ex) {
			handleRunFailure(context, ex, exceptionReporters, null);
			throw new IllegalStateException(ex);
		}
		return context;
	}
	

	//EventPublishingRunListener通过反射将application传入实例,
	public EventPublishingRunListener(SpringApplication application, String[] args) {
		this.application = application;
		this.args = args;
		this.initialMulticaster = new SimpleApplicationEventMulticaster();
		for (ApplicationListener<?> listener : application.getListeners()) {
			this.initialMulticaster.addApplicationListener(listener);
		}
	}

/**SpringApplicationRunListener是监听SpringApplication启动过程中处理事件
实现此类通过SpringFactoriesLoader反射实例化
并且须有一个接收两个参数分别为SpringApplication,String[]的构造方法
在springapplication启动过程中将Event推送事件到各个实现了SpringApplicationRunListener接口类处理
如有监听启动过程中的事件变化,可以实现此ApplicationListener接口(接口泛型为事件类型)添加到spring.factories上去.
*/
public interface SpringApplicationRunListener {
	//在启动时候run方法执行
	default void starting() {
	}
	//在环境准备好ApplicationContext初始化之前
	default void environmentPrepared(ConfigurableEnvironment environment) {
	}
	//在ApplicationContext初始化完成资源被加载之前
	default void contextPrepared(ConfigurableApplicationContext context) {
	}
	//资源被加载完成在容器刷新之前调用
	default void contextLoaded(ConfigurableApplicationContext context) {
	}
	//容器刷新完成应用启动完成,CommandLineRunners,ApplicationRunners被调用之前
	default void started(ConfigurableApplicationContext context) {
	}
	//容器刷新完成,CommandLineRunners,ApplicationRunners执行完成
	default void running(ConfigurableApplicationContext context) {
	}
	// 应用在启动或运行过程中失败后调用
	default void failed(ConfigurableApplicationContext context, Throwable exception) {
	}
}
	
```

主要来看看准备上下文,刷新上下文.

准备上下文prepareContext

1. 设置上下文环境
2. 初始化ApplicationContextInitializer
3. 调用所有SpringApplicationRunListener的contextPrepared方法
4. 配置ConfigurableListableBeanFactory,默认实现为DefaultListableBeanFactory
5. 加载资源,为启动时配置的类.
6. 调用所有SpringApplicationRunListener的contextLoaded方法

```java
//prepareContext
	private void prepareContext(ConfigurableApplicationContext context, ConfigurableEnvironment environment,
			SpringApplicationRunListeners listeners, ApplicationArguments applicationArguments, Banner printedBanner) {
        //设置上下文环境
		context.setEnvironment(environment);
		postProcessApplicationContext(context);

        //调用配置在spring.factories实现了ApplicationContextInitializer的类的initialize初始化，可以在context里面添加自定义BeanFactoryPostProcessor
		//默认有CachingMetadataReaderFactoryPostProcessor
		//ConfigurationWarningsPostProcessor
		//PropertySourceOrderingPostProcessor
		applyInitializers(context);

        //调用配置在spring.factories实现了SpringApplicationRunListener的类的contextPrepared方法
		listeners.contextPrepared(context);
		if (this.logStartupInfo) {
			logStartupInfo(context.getParent() == null);
			logStartupProfileInfo(context);
		}
		// ConfigurableListableBeanFactory 默认为创建时初始化的DefaultListableBeanFactory
		ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
		beanFactory.registerSingleton("springApplicationArguments", applicationArguments);
		if (printedBanner != null) {
			beanFactory.registerSingleton("springBootBanner", printedBanner);
		}
		if (beanFactory instanceof DefaultListableBeanFactory) {
			((DefaultListableBeanFactory) beanFactory)
					.setAllowBeanDefinitionOverriding(this.allowBeanDefinitionOverriding);
		}
		// 初始化资源,这里为传入的类SpringApplicationMain
		Set<Object> sources = getAllSources();
		Assert.notEmpty(sources, "Sources must not be empty");
        //加载资源类,如果是bean的话会注入到容器中
		load(context, sources.toArray(new Object[0]));
		listeners.contextLoaded(context);
	}
```

刷新容器会调用AbstractApplicationContext.refresh方法

```java
public void refresh() throws BeansException, IllegalStateException {
		synchronized (this.startupShutdownMonitor) {
			// 准备刷新,设置刷新标志位,initPropertySources,校验必须的属性依赖
			prepareRefresh();

			// Tell the subclass to refresh the internal bean factory.默认为DefaultListableBeanFactory,设置beanFactory的ID
			ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

			// Prepare the bean factory for use in this context.
            //初始化beanFactory（ 设置类加载器，设置BeanPostProcessor(ApplicationContextAwareProcessor,ApplicationListenerDetector等等)，设置ResourceLoader，设置ApplicationContext，注册几个特殊的 bean，例如SystemProperties，SystemEnvironment）
			prepareBeanFactory(beanFactory);

			try {
				// Allows post-processing of the bean factory in context subclasses.
                // 设置特殊的BeanPostProcessor(WebApplicationContextServletContextAwareProcessor,)，servletContext，servletConfig,WebRequest
				postProcessBeanFactory(beanFactory);

				// Invoke factory processors registered as beans in the context.
                // 调用BeanFactoryPostProcessor.postProcessBeanFactory
                // 实际调用BeanDefinitionRegistryPostProcessor.postProcessBeanDefinitionRegistry
                //或BeanFactoryPostProcessor.postProcessBeanFactory
				// BeanDefinitionRegistryPostProcessor是BeanFactoryPostProcessor子类
				invokeBeanFactoryPostProcessors(beanFactory);

				// Register bean processors that intercept bean creation.
                //设置特殊的BeanPostProcessor(BeanPostProcessorChecker)
				registerBeanPostProcessors(beanFactory);

				// Initialize message source for this context.
				//默认DelegatingMessageSource
				initMessageSource();

				// Initialize event multicaster for this context.
				//默认SimpleApplicationEventMulticaster
				initApplicationEventMulticaster();

				// Initialize other special beans in specific context subclasses.
				//子类重写此方法执行容器刷新后的操作
				onRefresh();

				// Check for listener beans and register them.
				//
				registerListeners();

				// Instantiate all remaining (non-lazy-init) singletons.
				// 实例化剩下的单例bean
				finishBeanFactoryInitialization(beanFactory);

				// Last step: publish corresponding event.
				finishRefresh();
			}

			catch (BeansException ex) {
				...
			}
			finally {
				...
			}
		}
	}
```
###invokeBeanFactoryPostProcessors
这里会调用prepareContext的applyInitializers添加的BeanFactoryPostProcessor实现类，将bean分成两类，分别是BeanDefinitionRegistryPostProcessor与BeanFactoryPostProcessor

如果当前的beanFactory是BeanDefinitionRegistry的实例，(默认DefaultListableBeanFactory实现了BeanDefinitionRegistry)
1. 先执行实现了PriorityOrdered接口并排序的BeanDefinitionRegistryPostProcessor.postProcessBeanDefinitionRegistry
然后执行实现了Ordered接口并排序的BeanDefinitionRegistryPostProcessor.postProcessBeanDefinitionRegistry
最后执行剩余的BeanDefinitionRegistryPostProcessors.postProcessBeanDefinitionRegistry
2. 调用BeanDefinitionRegistryPostProcessor，BeanFactoryPostProcessor的postProcessBeanFactory方法

如果当前的beanFactory不是是BeanDefinitionRegistry的实例，或者有剩下没有执行的BeanFactoryPostProcessor，则按以下顺序执行一遍
第一步执行实现了PriorityOrdered接口并排序的BeanFactoryPostProcessor.postProcessBeanFactory方法
第二步执行实现了Ordered接口并排序的BeanFactoryPostProcessor.postProcessBeanFactory方法
最后执行剩余的BeanFactoryPostProcessor.postProcessBeanFactory方法

```java
	public static void invokeBeanFactoryPostProcessors(
			ConfigurableListableBeanFactory beanFactory, List<BeanFactoryPostProcessor> beanFactoryPostProcessors) {
		...
		String[] postProcessorNames =
					beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
			for (String ppName : postProcessorNames) {
				if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
					currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
					processedBeans.add(ppName);
				}
			}
			sortPostProcessors(currentRegistryProcessors, beanFactory);
			invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);
		...
	}
	private static void invokeBeanDefinitionRegistryPostProcessors(
			Collection<? extends BeanDefinitionRegistryPostProcessor> postProcessors, BeanDefinitionRegistry registry) {
		// 循环调用BeanDefinitionRegistryPostProcessor的postProcessBeanDefinitionRegistry方法
		for (BeanDefinitionRegistryPostProcessor postProcessor : postProcessors) {
			postProcessor.postProcessBeanDefinitionRegistry(registry);
		}
	}
```
BeanDefinitionRegistryPostProcessor中
ConfigurationClassPostProcessor 处理@Configuration注解配置的类，在实现PriorityOrdered中优先级最低
实例化ConfigurationClassParser解析每个被@Configuration标记的类，查找可能被
1. 属性处理注解：PropertySources,PropertySource，初始化配置处理类工厂PropertySourceFactory
2. 包扫描注解ComponentScans，ComponentScan，扫描这些包下是否有同样的注解
3. 配置引用注解Import，ImportResource	保存被注解的类

代码处理位置ConfigurationClassParser.doProcessConfigurationClass(ConfigurationClass, SourceClass)
处理完后再调用postProcessBeanFactory对配置类代理增强,增强类(ConfigurationClassEnhancer)

接下来处理被Import注解的类
@Import注解可以是以下3中情况
1. 实现了ImportSelector接口的类，处理是会调用selectImports方法
2. 实现了ImportBeanDefinitionRegistrar接口的类，处理是会调用registerBeanDefinitions方法,这里可以动态注册bean
3. 不是1，2情况的，当成普通的Configuration类来处理

很多框架整合spring的方式可以结合1，2使用
如Hystrix的EnableHystrix使用的是方式1，对应的类为EnableCircuitBreakerImportSelector
如Feign的EnableFeignClients使用方式2，对应的类为FeignClientsRegistrar
如mybatits的MapperScan使用方式2，对应的类为MapperScannerRegistrar


###registerBeanPostProcessors(beanFactory)，主要是对bean的初始化前后处理;
在这方法前都可以添加BeanPostProcessor，可以在prepareContext的applyInitializers添加自定义的BeanPostProcessor
注册 BeanPostProcessorChecker日志记录，ApplicationListenerDetector在Bean创建完成后检查是否是ApplicationListener，如果是则加到context中applicationContext.addApplicationListener((ApplicationListener<?>) bean)
1. 获取所有实现了BeanPostProcessor接口的类，也是分成3类，分别为实现了PriorityOrdered，Ordered，其他没有排序的。
2. 将BeanPostProcessor分别加入到ConfigurableListableBeanFactory中
3. 将实现了MergedBeanDefinitionPostProcessor接口的实现类加入ConfigurableListableBeanFactory中，分别有CommonAnnotationBeanPostProcessor,AutowiredAnnotationBeanPostProcessor

###onRefresh
ServletWebServerApplicationContext 重写了onRefresh方法，调用父类创建一个ResourceBundleThemeSource后，创建一个server实例，默认创建tomcat服务ServletWebServerFactory有3个实现类，分别为JettyServletWebServerFactory，TomcatServletWebServerFactory，UndertowServletWebServerFactory
```java
	private void createWebServer() {
		WebServer webServer = this.webServer;
		ServletContext servletContext = getServletContext();
		if (webServer == null && servletContext == null) {
			//默认为Tomcat的话，获取beanname为[tomcatServletWebServerFactory]的实例TomcatServletWebServerFactory
			ServletWebServerFactory factory = getWebServerFactory();
			this.webServer = factory.getWebServer(getSelfInitializer());
		}
		else if (servletContext != null) {
			try {
				getSelfInitializer().onStartup(servletContext);
			}
			catch (ServletException ex) {
				throw new ApplicationContextException("Cannot initialize servlet context", ex);
			}
		}
		initPropertySources();
	}
```

###registerListeners();
将初始化实现了ApplicationListener接口的类添加到默认的广播器中ApplicationEventMulticaster（默认实现为SimpleApplicationEventMulticaster），ApplicationEventMulticaster与SpringApplicationRunListener不同
ApplicationEventMulticaster是应用的广播通知，可以自定义任何事件与任何时候进行
SpringApplicationRunListener是spring启动中的特定几个流程与事件
```java
	//添加listener
	getApplicationEventMulticaster().addApplicationListener(listener);
	//获取ApplicationListener
	getBeanNamesForType(ApplicationListener.class, true, false);
	//添加bean
	getApplicationEventMulticaster().addApplicationListenerBean(listenerBeanName);
```

###finishBeanFactoryInitialization
初始化剩下所有的单例bean，

1. 遍历beanDefinitionNames,非lazy加载的
2. 如果bean 是factoryBean,并且是SmartFactoryBean则根据SmartFactoryBean的isEagerInit方法来决定是否初始化调用getBean(beanName)方法,如果不是SmartFactoryBean则不初始化,其他情况调用getBean(beanName)初始化
3. 初始化完成后,如果bean是SmartInitializingSingleton,调用bean的afterSingletonsInstantiated方法

```java
//finishBeanFactoryInitialization 最终会调用beanFactory(DefaultListableBeanFactory)的preInstantiateSingletons方法
public void preInstantiateSingletons() throws BeansException {
    // Iterate over a copy to allow for init methods which in turn register new bean definitions.
		// While this may not be part of the regular factory bootstrap, it does otherwise work fine.
    //将所有注册的bean名称放入集合中
		List<String> beanNames = new ArrayList<>(this.beanDefinitionNames);

		// Trigger initialization of all non-lazy singleton beans...
    	// 初始化全部非lazy的bean
		for (String beanName : beanNames) {
            //获取RootBeanDefinition,如果不是RootBeanDefinition则转换成RootBeanDefinition病缓存
			RootBeanDefinition bd = getMergedLocalBeanDefinition(beanName);
			if (!bd.isAbstract() && bd.isSingleton() && !bd.isLazyInit()) {
                //是否工厂bean,是否实现FactoryBean结果
				if (isFactoryBean(beanName)) {
					Object bean = getBean(FACTORY_BEAN_PREFIX + beanName);
					if (bean instanceof FactoryBean) {
						final FactoryBean<?> factory = (FactoryBean<?>) bean;
						boolean isEagerInit;
						if (System.getSecurityManager() != null && factory instanceof SmartFactoryBean) {
							isEagerInit = AccessController.doPrivileged((PrivilegedAction<Boolean>)
											((SmartFactoryBean<?>) factory)::isEagerInit,
									getAccessControlContext());
						}
						else {
							isEagerInit = (factory instanceof SmartFactoryBean &&
									((SmartFactoryBean<?>) factory).isEagerInit());
						}
						if (isEagerInit) {
							getBean(beanName);
						}
					}
				}
				else {
					getBean(beanName);
				}
			}
		}

		// Trigger post-initialization callback for all applicable beans...
		for (String beanName : beanNames) {
			Object singletonInstance = getSingleton(beanName);
			if (singletonInstance instanceof SmartInitializingSingleton) {
				final SmartInitializingSingleton smartSingleton = (SmartInitializingSingleton) singletonInstance;
				if (System.getSecurityManager() != null) {
					AccessController.doPrivileged((PrivilegedAction<Object>) () -> {
						smartSingleton.afterSingletonsInstantiated();
						return null;
					}, getAccessControlContext());
				}
				else {
					smartSingleton.afterSingletonsInstantiated();
				}
			}
		}
}


```



####getBean 方法

1. 获取bean名称,如果是FactoryBean的话会带上&符号,去掉&符号
2. 从静态变量singletonObjects(ConcurrentHashMap)获取实例,如果获取到了,按以下处理，如果是工厂bean(实现了FactoryBean接口)，则调用FactoryBean的getObject方法来获取bean实例，并缓存到factoryBeanObjectCache的ConcurrentHashMap中，如果不是工厂bean则返回bean本身
3. 如果缓存不存在,委托parentBeanFactory获取bean,获取到后返回bean
4. parentBeanFactory也不存在,则将bean标记为在创建中,避免并发创建,双边检锁机制保证不会有并发问题
5. 获取bean的定义,检查依赖,递归处理依赖的bean,校验是否有循环依赖(在构造函数注入依赖可能导致循环依赖报错,用字段/setter注入方式则不会).
6. 依赖处理完后,分别处理不同的作用域的bean(singleton,prototype等等)

```java
//getBean方法
	public Object getBean(String name) throws BeansException {
		return doGetBean(name, null, null, false);
	}

		protected <T> T doGetBean(final String name, @Nullable final Class<T> requiredType,
			@Nullable final Object[] args, boolean typeCheckOnly) throws BeansException {
            ...
                //获取bean名称,如果是FactoryBean的话会带上&符号,要去掉&符号
                final String beanName = transformedBeanName(name);
            	//从静态变量singletonObjects(ConcurrentHashMap)获取实例,如果获取到了,按以下处理，如果是工厂bean(实现了FactoryBean接口)，则调用FactoryBean的getObject方法来获取bean实例，并缓存到factoryBeanObjectCache的ConcurrentHashMap中，如果不是工厂bean则返回bean本身
            	Object sharedInstance = getSingleton(beanName);
            	if (sharedInstance != null && args == null) {
                   	bean = getObjectForBeanInstance(sharedInstance, name, beanName, null);
                }else {
                    //如果不存在的话,获取parentBeanFactory,DefaultListableBeanFactory实现了了HierarchicalBeanFactory接口
                    BeanFactory parentBeanFactory = getParentBeanFactory();
                    if (parentBeanFactory != null && !containsBeanDefinition(beanName)) {
                        // 存在parentBeanFactory且当前BeanFactory不存在bean,从parentBeanFactory获取bean
                        String nameToLookup = originalBeanName(name);
                        // 委托parentBeanFactory获取bean
                        if (parentBeanFactory instanceof AbstractBeanFactory) {
                            return ((AbstractBeanFactory) parentBeanFactory).doGetBean(
                                    nameToLookup, requiredType, args, typeCheckOnly);
                        }
                        else if (args != null) {
                            return (T) parentBeanFactory.getBean(nameToLookup, args);
                        }
                        else if (requiredType != null) {
                            // No args -> delegate to standard getBean method.
                            return parentBeanFactory.getBean(nameToLookup, requiredType);
                        }
                        else {
                            return (T) parentBeanFactory.getBean(nameToLookup);
                        }
                    }
                    if (!typeCheckOnly) {
                  //标记当前bean已创建,里面会同步mergedBeanDefinitions,加入alreadyCreated:Set
                        markBeanAsCreated(beanName);
                    }
                 	//获取bean定义
                    final RootBeanDefinition mbd = getMergedLocalBeanDefinition(beanName);
					//校验,不能是abstract
                    checkMergedBeanDefinition(mbd, beanName, args);
                    //处理依赖,如果当前bean依赖其他bean,则将依赖的bean注册到容器中,将依赖的bean实例化
                    String[] dependsOn = mbd.getDependsOn();
                    if (dependsOn != null) {
                        for (String dep : dependsOn) {
                            // 检查是否有循环依赖
                            if (isDependent(beanName, dep)) {
                                throw new BeanCreationException
                            }
                            //注册依赖的bean,递归处理bean依赖
                            registerDependentBean(dep, beanName);
                            try {
                                getBean(dep);
                            }
                            catch (NoSuchBeanDefinitionException ex) {
                                throw new BeanCreationException;
                            }
                        }
                    }
                    
                    // 创建单例的bean
                    if (mbd.isSingleton()) {
                        sharedInstance = getSingleton(beanName, () -> {
                            try {
                                return createBean(beanName, mbd, args);
                            }
                            catch (BeansException ex) {
                                ....
                                throw ex;
                            }
                        });
                        bean = getObjectForBeanInstance(sharedInstance, name, beanName, mbd);
                    }
                    //prototype,创建前加入prototypesCurrentlyInCreation:ThreadLocal，表明bean在创建中（用于检查循环依赖的问题）,创建后清除ThreadLocal的值
                    else if (mbd.isPrototype()) {
                        Object prototypeInstance = null;
                        try {
                            beforePrototypeCreation(beanName);
                            prototypeInstance = createBean(beanName, mbd, args);
                        }
                        finally {
                            afterPrototypeCreation(beanName);
                        }
                        bean = getObjectForBeanInstance(prototypeInstance, name, beanName, mbd);
                    }
                    //其他类型(RequestScope,SessionScope,RefreshScope,ServletContextScope)跟prototype类似,创建出来的实例放在对应的scope里面,处理了IllegalStateException异常
                    else {
                        String scopeName = mbd.getScope();
                        final Scope scope = this.scopes.get(scopeName);
                        if (scope == null) {
                            throw new IllegalStateException("No Scope registered for scope name '" + scopeName + "'");
                        }
                        try {
                            Object scopedInstance = scope.get(beanName, () -> {
                                beforePrototypeCreation(beanName);
                                try {
                                    return createBean(beanName, mbd, args);
                                }
                                finally {
                                    afterPrototypeCreation(beanName);
                                }
                            });
                            bean = getObjectForBeanInstance(scopedInstance, name, beanName, mbd);
					}
					catch (IllegalStateException ex) {
						...
						throw new BeanCreationException
					}
				}
        }
        
```

createBean创建bean实例

```java
	// 创建实例方法,AbstractBeanFactory实现方法
    protected Object createBean(String beanName, RootBeanDefinition mbd, @Nullable Object[] args)
    throws BeanCreationException {
        ...
        //实例化之前调用用BeanPostProcessor来处理,可以在这里来创建一个代理对象,如果能实例化成功,返回bean,如果返回为空则使用普通方法创建
        Object bean = resolveBeforeInstantiation(beanName, mbdToUse);
        if (bean != null) {
            return bean;
        }
        //普通方法实例化bean,通过反射方法来实例化,实例化完成后调用MergedBeanDefinitionPostProcessor.postProcessMergedBeanDefinition方法,如
		//AutowiredAnnotationBeanPostProcessor可以处理@Autowired,@Value,@Inject注解,调populateBean(beanName, mbd, instanceWrapper)将属性注入
        Object beanInstance = doCreateBean(beanName, mbdToUse, args);
        return beanInstance;
    }
	
	//实例化之前解析bean
	protected Object resolveBeforeInstantiation(String beanName, RootBeanDefinition mbd) {
		Object bean = null;
		if (!Boolean.FALSE.equals(mbd.beforeInstantiationResolved)) {
			// Make sure bean class is actually resolved at this point.
			if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
				Class<?> targetType = determineTargetType(beanName, mbd);
				if (targetType != null) {
                    //调用InstantiationAwareBeanPostProcessor.postProcessBeforeInstantiation方法
					bean = applyBeanPostProcessorsBeforeInstantiation(targetType, beanName);
					if (bean != null) {
						bean = applyBeanPostProcessorsAfterInitialization(bean, beanName);
					}
				}
			}
			mbd.beforeInstantiationResolved = (bean != null);
		}
		return bean;
	}
```

所有的bean实例化完后,回到AnnotationConfigServletWebServerApplicationContext->ServletWebServerApplicationContext->AbstractApplicationContext,执行finishRefresh();

```java
	//ServletWebServerApplicationContext.finishRefresh
	protected void finishRefresh() {
		super.finishRefresh();
        //启动webserver
		WebServer webServer = startWebServer();
		if (webServer != null) {
            //广播事件,WebServer初始化完成事件,调用所有listener的listener.onApplicationEvent(event);,对ServletWebServerInitializedEvent感兴趣的可以判断类型做相关操作
			publishEvent(new ServletWebServerInitializedEvent(webServer, this));
		}
	}

	//AbstractApplicationContext.finishRefresh
	protected void finishRefresh() {
		// Clear context-level resource caches (such as ASM metadata from scanning).
        //清理资源
		clearResourceCaches();

		// Initialize lifecycle processor for this context.
        //注册一个beanName为lifecycleProcessor的bean,如果不存在的话,会实例化DefaultLifecycleProcessor
		initLifecycleProcessor();

		// Propagate refresh to lifecycle processor first.
        //调用lifecycleProcessor的onRefresh方法,启动相关的bean,bean实现了Lifecycle接口
		getLifecycleProcessor().onRefresh();

		// Publish the final event.
        //广播事件,上下文刷新完成,调用所有listener的listener.onApplicationEvent(event);,对ContextRefreshedEvent感兴趣的可以判断类型做相关操作
		publishEvent(new ContextRefreshedEvent(this));

		// Participate in LiveBeansView MBean, if active.
		LiveBeansView.registerApplicationContext(this);
	}


```



DefaultLifecycleProcessor的onRefresh方法

```java
//DefaultLifecycleProcessor的onRefresh方法
public void onRefresh() {
	startBeans(true);
	this.running = true;
}
//启动相关的bean
private void startBeans(boolean autoStartupOnly) {
    //获取类型为Lifecycle的bean
	Map<String, Lifecycle> lifecycleBeans = getLifecycleBeans();
	Map<Integer, LifecycleGroup> phases = new HashMap<>();
	lifecycleBeans.forEach((beanName, bean) -> {
        //是SmartLifecycle接口,对bean分组,如果没实现phase接口,默认都是在0组
		if (!autoStartupOnly || (bean instanceof SmartLifecycle && ((SmartLifecycle) bean).isAutoStartup())) {
			int phase = getPhase(bean);
			LifecycleGroup group = phases.get(phase);
			if (group == null) {
				group = new LifecycleGroup(phase, this.timeoutPerShutdownPhase, lifecycleBeans, autoStartupOnly);
				phases.put(phase, group);
			}
			group.add(beanName, bean);
		}
	});
    //调用bean的start方法,按key优先级排序
	if (!phases.isEmpty()) {
		List<Integer> keys = new ArrayList<>(phases.keySet());
		Collections.sort(keys);
		for (Integer key : keys) {
			phases.get(key).start();
		}
	}
}
```

ConfigurationClassPostProcessor		
处理配置注解
Configuration,
Component(Controller,Service,Repository,声明为组件),
PropertySources,PropertySource(将配置文件写入到当前环境EnableAutoConfiguration中,可以使用占位符${}替换),
ComponentScans,ComponentScan(spring组件扫描包路径,将扫描到的bean类注册到beanFactory上,处理类ClassPathBeanDefinitionScanner.doScan,如有Configuration注解的类会递归解析),如果没有定义basePackages,或basePackageClasses,则用当期指定的source类作为包名
Import,ImportResource, 引入配置资源,ImportResource(可以指定xml等资源配置文件,parser.parse(candidates)过程中解析新加入的bean)
Bean解析完成后调用ConfigurationClassBeanDefinitionReader.loadBeanDefinitions(configClasses)解析bean注解
Scope(在处理扫描类会处理Scope注解)
Lazy(在createBean时候处理)
2. InitDestroyAnnotationBeanPostProcessor  
bean初始化,销毁注解处理,处理PostConstruct,PreDestroy注解
3. CommonAnnotationBeanPostProcessor 	
通用处理器主要处理JSR-250,EJB注解处理,如WebServiceRef,EJB,Resource注解
4. AutowiredAnnotationBeanPostProcessor 	
AutoWired,Value,Inject注解处理


BeanFactory 所有bean集合,保存了所有注册的bean,getBean返回bean的实例
FactoryBean 产生某种特殊bean的一个bean,注册到BeanFactory的一种bean,在创建bean的时候会调用其getObject返回实例.
BeanFactoryPostProcessor: 从各个地方(注解,xml,配置等等)生成bean注册到beanFactory上,如ConfigurationClassPostProcessor,处理配置类
BeanPostProcessor: 实例化bean前后处理逻辑,在createBean时候被调用

bean的Aware接口,在需要的地方织入对应的属性.
例如:在bean实例化之后,调用 ApplicationContextAwareProcessor.postProcessBeforeInitialization 处理bean的时候调用实现了Aware接口的方法.
```java
//org.springframework.context.support.ApplicationContextAwareProcessor.invokeAwareInterfaces(Object)
	private void invokeAwareInterfaces(Object bean) {
		if (bean instanceof Aware) {
			if (bean instanceof EnvironmentAware) {
				((EnvironmentAware) bean).setEnvironment(this.applicationContext.getEnvironment());
			}
			if (bean instanceof EmbeddedValueResolverAware) {
				((EmbeddedValueResolverAware) bean).setEmbeddedValueResolver(this.embeddedValueResolver);
			}
			if (bean instanceof ResourceLoaderAware) {
				((ResourceLoaderAware) bean).setResourceLoader(this.applicationContext);
			}
			if (bean instanceof ApplicationEventPublisherAware) {
				((ApplicationEventPublisherAware) bean).setApplicationEventPublisher(this.applicationContext);
			}
			if (bean instanceof MessageSourceAware) {
				((MessageSourceAware) bean).setMessageSource(this.applicationContext);
			}
			if (bean instanceof ApplicationContextAware) {
				((ApplicationContextAware) bean).setApplicationContext(this.applicationContext);
			}
		}
	}

```

类结构:

 Class hierarchy
 - Aware
   - ApplicationContextAware(ApplicationContext)
   - ApplicationEventPublisherAware(ApplicationEventPublisher)
   - BeanClassLoaderAware(ClassLoader)
   - BeanFactoryAware(BeanFactory)
   - BeanNameAware(String beanName)
   - BootstrapContextAware(BootstrapContext)
   - EmbeddedValueResolverAware(StringValueResolver)
   - EnvironmentAware(Environment)
   - ImportAware(AnnotationMetadata)
   - LoadTimeWeaverAware(LoadTimeWeaver)
   - MessageSourceAware(MessageSource)
   - NotificationPublisherAware(NotificationPublisher)
   - ResourceLoaderAware(ResourceLoader)
   - SchedulerContextAware(SchedulerContext)
   - ServletConfigAware(ServletConfig)
   - ServletContextAware(ServletContext)

###Conditional条件配置
@Conditional是其他ConditionalOnXXX的父注解类,这个类的方法指明用哪个OnXXCondition来过滤配置时候满足条件.
1. OnBeanCondition处理注解(@ConditionalOnBean,@ConditionalOnMissingBean,@ConditionalOnSingleCandidate),分别表示当指定的bean存在/不存在/beanFactory容器中只存在一个时候生效.
2. OnClassCondition处理注解(@ConditionalOnClass,ConditionalOnMissingClass)
3. OnWebApplicationCondition处理注解@ConditionalOnWebApplication,当应用是WebApplicationContext时候配置生效
4. OnPropertyCondition处理注解(@ConditionalOnProperty),当环境中存在某个指定配置时候次配置生效.
5. OnJavaCondition处理注解(@ConditionalOnJava),当java运行的版本满足条件时配置生效.
6. OnResourceCondition处理注解(@OnResourceCondition),当存在资源文件时候配置生效.

@AutoConfigureBefore,@AutoConfigureAfter: 指定配置启用顺序,表示当前配置需要在某个配置前或后,自动配置时候除了根据Order接口排序,还会根据这两注解来排序.
 
在实际使用场景中,依赖的第三方插件位于jar包中,自身写的配置位于应用中,容器会优先执行class目录下的配置,然后再找jar/class下的配置,class下的配置优先于jar包的配置.



###设计模式:
1. 工厂模式:如FactoryBean
2. 单例,bean默认是单例
3. 观察者模式: ApplicationListener,SpringApplicationRunListener(观察初始化事件)
4. 模板:JdbcTemplate,RedisTemplate(定义模板处理方法)
5. 代理模式,Aop
6. 策略模式:InstantiationStrategy(bean初始化策略)
7. 装饰者模式:BeanDefinitionDecorator(给bean添加)
8. 适配器模式:AdvisorAdapter(适配代理方法)


spring环形依赖无法解决的情况：
1. 构造方法注入
2. 多实例的循环依赖，并且在启动时初始化
3. 环形依赖中早期对象被代理导致引用发生变化，比如ServiceA，serviceB相互依赖，如果先初始化ServiceA再初始化serviceB，那么serviceB得到的是一个早期不完整的对象(在earlySingletonObjects中)，此时如果ServiceA被代理(比如Cglib代理)，那么引用值发生变化，serviceB得到的就不是ServiceA的实际引用，因此会报错；但是如果调换顺序，serviceB先初始化(没有发生被代理情况)则没有问题，可以在ServiceA上打注解@DependsOn(serviceB)，表示serviceB先初始化再到ServiceA。
4. DependsOn注解的循环依赖，比如ServiceA有个注解@DependsOn(serviceB),SeriveB有个注解@DependsOn(serviceA) 则会发生报错