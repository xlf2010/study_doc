##gdb debug
1. 常用命令

gdb executed_file  启动gdb
set	args 设置运行参数
start  启动停在main函数
run/r  运行文件
break/b 设置断点，可以是函数名或文件行数，后面可以加条件,如在一个循环内，b 33 if i==10,则在i=10情况下触发断点
print/p  打印变量值或表达式结果，数组的话可以使用这个打印全部元素p *arr_name@length
list/l  显示代码行数与位置
watch  监控变量，有变化会显示出来
disassemble  显示汇编代码
backtrace/bt 显示当前执行的函数栈信息
info 打印信息
info breakpoints 显示断点信息
info threads 显示线程信息
info frame  显示栈帧信息

disable 取消相关配置
disable breakpoints 设置断点失效

enable 启用相关配置
enable breakpoints 设置启用断点

运行控制：
continue/c  从断点处继续运行，执行到下一断点或结束
finish  运行到函数返回return
return 从当前指令执行return，
step/s  执行下一行，如有函数调用会进入到函数体执行
stepi/si 执行下一条指令，如有函数调用会进入到函数体执行,为函数的首指令
next/n  执行当前函数下一行，如函数调用不会进入函数体执行
nexti/ni  执行下一条指令，如函数调用不会进入函数体执行，当前函数的下一指令
signal 向当前调试程序发送信号
kill kill掉调试进程，相当于执行kill命令
