##mysql存储方式
1. decimal
decimal(M,N)，M为总位数，N为小数位，例如decimal(20,6)则包括14位整数部分，6位小数部分。默认值M=10，N=0，最大值M=65
*特别的* decimal(M)=decimal(M,0),decimal=decimal=decimal(10,0)
存储方式：
按小数点分成两部分，每九个十进制用4个字节存储，不足九位的空间按以下方式：

|位数长度|字节占用数|
-|-|-
|0|0|
|1-2|1|
|3-4|2|
|5-6|3|
|7-8|4|

例如decimal(20,6),整数位数14位，按照每九位4字节算，则有14=9+5，总共需要4+3字节，小数部分为6位，总共需要3字节，因此decimal(20,6)需要10字节来存储。

----
###mysql索引问题
1. mysql索引关联失效，关联条件类型要相同，字符串的话最好是编码，长度一致。


###mysql count(1),count(\*),count(column)
[https://dev.mysql.com/doc/refman/5.7/en/group-by-functions.html#function_count](https://dev.mysql.com/doc/refman/5.7/en/group-by-functions.html#function_count)
count(\*) 返回匹配的行数，在多事务中可能存在问题，每个事务在同一时间可能返回的值不一样。Innodb没有在表定义中保存记录数，MyISAM 在没有where条件直接返回记录数。
在InnoDB存储引擎中，count(1),count(\*)处理逻辑一样
MyISAM 中，COUNT(1)要达到与count(\*)一样效率的前提是第一列定义为NOT NULL。

###GROUP\_CONCAT
group concat将group by分组的列名连接起来，其中不包括NULL值。
```sql
GROUP_CONCAT([DISTINCT] expr [,expr ...]
             [ORDER BY {unsigned_integer | col_name | expr}
                 [ASC | DESC] [,col_name ...]]
             [SEPARATOR str_val])
#例如：
	SELECT student_name,
         GROUP_CONCAT(DISTINCT test_score
                      ORDER BY test_score DESC SEPARATOR ' ')
       FROM student
       GROUP BY student_name;
#DISTINCT 去重
#order by 排序
#separator 连接分隔符，默认是','
```


事务特性
ACID: